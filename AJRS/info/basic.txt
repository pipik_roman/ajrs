Toto sa bude vykonávať vždy, pretože z toho vychádzajú ostatné princípy:
filtrovanie AST: používame iba:

class declaration
method declaration (parametre,nazov)
field declaration (typ,nazov)

method invocation ()
field set/get ?

(pre compilation unit):
1. aplikovať na metódy, rozhrania a polia transformačnú funkciu - Ďalej používame iba čísla, nie AST 

(pre každú class definition):
1. vytvorenie zoznamu rozhraní
2. vytvorenie zoznamu metód
3. vytvorenie zoznamu polí

(pre každú method declaration):
1. priradenie metódy do rozhrania
2. priradenie polí do metódy
3. priradenie polí do rozhrania

(pre každú class definition):
1.hľadať double personality

(pre každý double personality aspect):
1. označiť rozhranie aspektom
2. označiť metódy rozhrania aspektom
3. označiť polia ktoré sa používajú v označených rozhraniach aspektom

>copilation unit
>>class
>>>superclass declaration
>>>interface declaration
>>>field declaration
>>>method declaration
>>>>method invocation
>>>>field set/get
