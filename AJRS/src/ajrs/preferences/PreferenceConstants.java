package ajrs.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {
	// AJRS settings:
	public static final String GENERATE_STATEMENT_DUPLICATION_DATA = "GENERATE_STATEMENT_DUPLICATION_DATA";
	public static final String GENERATE_STATEMENT_HASH_MARK = "GENERATE_STATEMENT_HASH_MARK";
	// nastavenia pre complexity
	public static final String INPUT_VARIABLE_COEF = "INPUT_VARIABLE_COEF";
	public static final String OUTPUT_VARIABLE_COEF = "OUTPUT_VARIABLE_COEF";

	public static final String COMPLEXITY_BODY = "COMPLEXITY_COEFICIENT_BODY";
	public static final String COMPLEXITY_BREAK_CONTINUE = "COMPLEXITY_COEFICIENT_BREAK_CONTINUE";
	public static final String COMPLEXITY_CONDITIONAL = "COMPLEXITY_COEFICIENT_CONDITIONAL";
	public static final String COMPLEXITY_EXPRESSION = "COMPLEXITY_COEFICIENT_EXPRESSION";
	public static final String COMPLEXITY_LOOP = "COMPLEXITY_COEFICIENT_LOOP";
	public static final String COMPLEXITY_RETURN = "COMPLEXITY_COEFICIENT_RETURN";
	public static final String COMPLEXITY_THROW = "COMPLEXITY_COEFICIENT_THROW";
	public static final String COMPLEXITY_TRY = "COMPLEXITY_COEFICIENT_TRY";
	public static final String COMPLEXITY_VARIABLE = "COMPLEXITY_COEFICIENT_VARIABLE";

	public static final String SIZE_COMPLEXITY_LIMIT = "SIZE_COMPLEXITY_LIMIT";
	public static final String DEPTH_COMPLEXITY_LIMIT = "DEPTH_COMPLEXITY_LIMIT";

	public static final String TOO_MANY_ARGUMENTS_LIMIT = "TOO_MANY_ARGUMENTS_LIMIT";
	public static final String LONG_METHOD_SIZE_LIMIT = "LONG_METHOD_SIZE_LIMIT";

	public static final String LARGE_CLASS_METHOD_LIMIT = "LARGE_CLASS_METHOD_LIMIT";
	public static final String LARGE_CLASS_FIELD_LIMIT = "LARGE_CLASS_FIELD_LIMIT";

	public static final String LAZY_CLASS_METHOD_LIMIT = "LAZY_CLASS_METHOD_LIMIT";
	public static final String LAZY_CLASS_FIELD_LIMIT = "LAZY_CLASS_FIELD_LIMIT";

	public static final String IF_SIZE_LIMIT = "IF_SIZE_LIMIT";
	public static final String SWITCH_SIZE_LIMIT = "SWITCH_SIZE_LIMIT";

	public static final String DUPLICATION_MINIMUM = "DUPLICATION_MINIMUM";
}
