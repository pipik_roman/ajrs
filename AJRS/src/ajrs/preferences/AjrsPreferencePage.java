package ajrs.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import ajrs.Activator;

/**
 * This class represents a preference page that is contributed to the Preferences dialog. By subclassing <samp>FieldEditorPreferencePage</samp>, we can use the field support built into JFace that
 * allows us to create a page that is small and knows how to save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They are stored in the preference store that belongs to the main plug-in class. That way, preferences can be accessed directly via the preference
 * store.
 */

public class AjrsPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	public AjrsPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("AspectJ Refactoring support preferences");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI blocks needed to manipulate various types of preferences. Each field editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {
		// addField(new BooleanFieldEditor(PreferenceConstants.GENERATE_STATEMENT_HASH_MARK, "Generate statement hash marks in code: ", getFieldEditorParent()));

		addField(new IntegerFieldEditor(PreferenceConstants.DUPLICATION_MINIMUM, "Minimum duplications for duplicated code: ", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.TOO_MANY_ARGUMENTS_LIMIT, "Maximum number of method arguments: ", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.IF_SIZE_LIMIT, "Maximum size of if statement: ", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.SWITCH_SIZE_LIMIT, "Maximum size of switch statement: ", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.LONG_METHOD_SIZE_LIMIT, "Length of method to be too long: ", getFieldEditorParent()));

		addField(new IntegerFieldEditor(PreferenceConstants.DEPTH_COMPLEXITY_LIMIT, "Code is complex in depth when scored over: ", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.SIZE_COMPLEXITY_LIMIT, "Code is complex in size when scored over: ", getFieldEditorParent()));

		addField(new IntegerFieldEditor(PreferenceConstants.LARGE_CLASS_FIELD_LIMIT, "Large class has more field than: ", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.LARGE_CLASS_METHOD_LIMIT, "Large class has more methods than: ", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.LAZY_CLASS_FIELD_LIMIT, "Lazy class has less field than: ", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.LAZY_CLASS_METHOD_LIMIT, "Lazy class has less methods than: ", getFieldEditorParent()));

		addField(new IntegerFieldEditor(PreferenceConstants.INPUT_VARIABLE_COEF, "Complexity for input (read) variable", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.OUTPUT_VARIABLE_COEF, "Complexity for output (write) variable", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.COMPLEXITY_BODY, "Complexity for body statement ({})", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.COMPLEXITY_BREAK_CONTINUE, "Complexity for break and continue statements", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.COMPLEXITY_CONDITIONAL, "Complexity for conditional statement (if, switch, case)", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.COMPLEXITY_EXPRESSION, "Complexity for expression", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.COMPLEXITY_LOOP, "Complexity for loop (for, while, do while)", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.COMPLEXITY_RETURN, "Complexity for return statement", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.COMPLEXITY_THROW, "Complexity for throw statement", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.COMPLEXITY_TRY, "Complexity for try statement", getFieldEditorParent()));
		addField(new IntegerFieldEditor(PreferenceConstants.COMPLEXITY_VARIABLE, "Complexity for variable declaration statement", getFieldEditorParent()));
	}

	@Override
	public void init(IWorkbench workbench) {
		// ignore
	}

}
