package ajrs.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import ajrs.Activator;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.GENERATE_STATEMENT_HASH_MARK, true);
		store.setDefault(PreferenceConstants.INPUT_VARIABLE_COEF, 1);
		store.setDefault(PreferenceConstants.INPUT_VARIABLE_COEF, 1);
		store.setDefault(PreferenceConstants.OUTPUT_VARIABLE_COEF, 1);
		store.setDefault(PreferenceConstants.COMPLEXITY_BODY, 0);
		store.setDefault(PreferenceConstants.COMPLEXITY_BREAK_CONTINUE, 1);
		store.setDefault(PreferenceConstants.COMPLEXITY_CONDITIONAL, 2);
		store.setDefault(PreferenceConstants.COMPLEXITY_EXPRESSION, 1);
		store.setDefault(PreferenceConstants.COMPLEXITY_LOOP, 2);
		store.setDefault(PreferenceConstants.COMPLEXITY_RETURN, 2);
		store.setDefault(PreferenceConstants.COMPLEXITY_THROW, 2);
		store.setDefault(PreferenceConstants.COMPLEXITY_TRY, 2);
		store.setDefault(PreferenceConstants.COMPLEXITY_VARIABLE, 1);

		store.setDefault(PreferenceConstants.TOO_MANY_ARGUMENTS_LIMIT, 5);
		store.setDefault(PreferenceConstants.LONG_METHOD_SIZE_LIMIT, 20);
		store.setDefault(PreferenceConstants.DEPTH_COMPLEXITY_LIMIT, 10);
		store.setDefault(PreferenceConstants.SIZE_COMPLEXITY_LIMIT, 30);
		store.setDefault(PreferenceConstants.LARGE_CLASS_FIELD_LIMIT, 5);
		store.setDefault(PreferenceConstants.LARGE_CLASS_METHOD_LIMIT, 15);
		store.setDefault(PreferenceConstants.LAZY_CLASS_FIELD_LIMIT, 1);
		store.setDefault(PreferenceConstants.LAZY_CLASS_METHOD_LIMIT, 3);
		store.setDefault(PreferenceConstants.SIZE_COMPLEXITY_LIMIT, 20);
		store.setDefault(PreferenceConstants.IF_SIZE_LIMIT, 5);
		store.setDefault(PreferenceConstants.SWITCH_SIZE_LIMIT, 10);

		store.setDefault(PreferenceConstants.DUPLICATION_MINIMUM, 2);
	}
}
