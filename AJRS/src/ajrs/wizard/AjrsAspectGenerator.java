package ajrs.wizard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Type;

import ajrs.Require;
import ajrs.data.AjrsField;
import ajrs.data.AjrsMethod;
import ajrs.data.AjrsObject;
import ajrs.data.AjrsObjectVisitor;
import ajrs.data.AjrsUnit;
import ajrs.data.smells.duplicatedCode.AjrsAopDuplicatedCode;
import ajrs.data.smells.duplicatedCode.AjrsDuplicatedCode;
import ajrs.data.statement.AjrsBodyStatement;
import ajrs.data.statement.AjrsBreakContinueStatement;
import ajrs.data.statement.AjrsConditionalStatement;
import ajrs.data.statement.AjrsExpressionStatement;
import ajrs.data.statement.AjrsLoopStatement;
import ajrs.data.statement.AjrsMethodBlock;
import ajrs.data.statement.AjrsReturnStatement;
import ajrs.data.statement.AjrsStatement;
import ajrs.data.statement.AjrsThrowStatement;
import ajrs.data.statement.AjrsTryStatement;
import ajrs.data.statement.AjrsVariableStatement;

public class AjrsAspectGenerator extends AjrsObjectVisitor {
	private static String lineSeparator = System.getProperty("line.separator");
	private final StringBuilder builder = new StringBuilder();
	private final String aspectName;
	final String interfaceName;
	private boolean finished;
	private final Map<String, List<String>> pointcutNameToParams = new HashMap<String, List<String>>();
	private final Map<String, String> pointcutNameToPointcut = new HashMap<String, String>();
	private final Set<String> imports = new HashSet<String>();
	private final List<String> advices = new ArrayList<String>();
	private final List<String> methods = new ArrayList<String>();
	private String intertypeDeclarations = null;

	int pointcutCounter = 0;
	int tabsCounter = 0;

	public AjrsAspectGenerator(String aspectName) {
		this.aspectName = aspectName;
		this.interfaceName = "Has" + aspectName + "Interface";
	}

	@Override
	public boolean visit(AjrsBodyStatement statement) {
		start();
		return super.visit(statement);
	}

	@Override
	public void visit(AjrsBreakContinueStatement statement) {
		add(statement.getPeer().toString());
	}

	@Override
	public boolean visit(AjrsMethodBlock statement) {
		throw new AssertionError("We are not exporting method blocks for now!");
	}

	@Override
	public boolean visit(AjrsMethod method) {
		throw new AssertionError("We are not exporting methods for now!");
	}

	@Override
	public void afterVisit(AjrsMethod method) {
		throw new AssertionError("We are not exporting methods for now!");
	}

	@Override
	public void afterVisit(AjrsMethodBlock statement) {
		throw new AssertionError("We are not exporting method blocks for now!");
	}

	@Override
	public void afterVisit(AjrsBodyStatement statement) {
		end();
	}

	/**
	 * Process informations collected into resulting string;
	 */
	public void finish() {
		checkNotFinished();
		// add imports
		for (String name : imports) {
			add("import ").add(name).add(';').nl();
		}
		// start aspect
		add("public aspect ").add(aspectName).start();
		// add instance interface
		add(intertypeDeclarations);
		for (String pointcutName : pointcutNameToParams.keySet()) {
			add(pointcutNameToPointcut.get(pointcutName)).nl();
		}
		for (String advice : advices) {
			add(advice).nl();
		}
		for (String method : methods) {
			add(method).nl();
		}
		// end aspect
		end();
		finished = true;
	}

	public byte[] getBytes() {
		return toString().getBytes();
	}

	@Override
	public boolean visit(AjrsConditionalStatement statement) {
		add(statement.getPeer().toString());
		return super.visit(statement);
	}

	@Override
	public void visit(AjrsExpressionStatement statement) {
		add(statement.getPeer().toString());
	}

	@Override
	public void visit(AjrsField field) {
		throw new AssertionError("We are not visiting fields here");
	}

	@Override
	public boolean visit(AjrsLoopStatement statement) {
		add(statement.getPeer().toString());
		return super.visit(statement);
	}

	@Override
	public void visit(AjrsReturnStatement statement) {
		add(statement.getPeer().toString());
	}

	@Override
	public void visit(AjrsThrowStatement statement) {
		add(statement.getPeer().toString());
	}

	@Override
	public boolean visit(AjrsTryStatement statement) {
		add(statement.getPeer().toString());
		return super.visit(statement);
	}

	public boolean visit(ajrs.data.AjrsType type) {
		throw new AssertionError("We are not visiting AjrsType");
	};

	@Override
	public boolean visit(AjrsUnit unit) {
		throw new AssertionError("We are not visiting AjrsType");
	}

	@Override
	public void visit(AjrsVariableStatement statement) {
		add(statement.getPeer().toString());
	}

	@Override
	public String toString() {
		checkFinished();
		return builder.toString();
	}

	public String createPointcut(String pointcutDef, List<String> params, List<String> args) {
		checkNotFinished();
		clear();
		String pointcutName = "pointcut" + ++pointcutCounter;
		add("public pointcut ").add(pointcutName).add("(");
		if (params != null) {
			for (int i = 0; i < params.size(); ++i) {
				add(params.get(i));
				if (i + 1 < params.size()) {
					add(", ");
				}
			}
		}

		add(")");
		add(": ").add(pointcutDef);
		if (args != null) {
			add(" && args(");
			for (int i = 0; i < args.size(); ++i) {
				add(args.get(i));
				if (i + 1 < args.size()) {
					add(", ");
				}
			}
			add(")");
		}
		add(";").nl();
		pointcutNameToParams.put(pointcutName, params);
		pointcutNameToPointcut.put(pointcutName, builder.toString());
		clear();
		return pointcutName;
	}

	public void createDuplicateAdvice(String adviceType, String pointcutName, AjrsStatement node) {
		clear();
		checkNotFinished();
		Require.requireOneOf(adviceType, "adviceType", "before", "after", "around");
		Require.notNull(node, "ASTNode");
		add(adviceType).add("(");
		List<String> pointcutParams = pointcutNameToParams.get(pointcutName);
		if (pointcutParams != null) {
			for (int i = 0; i < pointcutParams.size(); ++i) {
				add(pointcutParams.get(i));
				if (i + 1 < pointcutParams.size()) {
					add(", ");
				}
			}
		}
		add(" ) ");
		add(" : ");
		add(pointcutName).add("(");
		if (pointcutParams != null) {
			for (int i = 0; i < pointcutParams.size(); ++i) {
				String pointcutParam = pointcutParams.get(i);
				String pointcutParamName = pointcutParam.substring(pointcutParam.indexOf(' ') + 1);
				add(pointcutParamName);
				if (i + 1 < pointcutParams.size()) {
					add(", ");
				}
			}
		}
		add(")");
		begin();
		add("adviceImplementation(objectWithAspectInterface");
		List<AjrsField> fields = new ArrayList<AjrsField>(node.getFields());

		if (fields.size() > 0) {
			add(", ");
		}
		for (int i = 0; i < fields.size(); ++i) {
			addQuoted(fields.get(i).getName());
			if (i + 1 < fields.size()) {
				add(", ");
			}
		}
		add(");").nl();
		end();
		advices.add(builder.toString());
		clear();
	}

	public void defineIntertypeDeclarations(AjrsDuplicatedCode smell) {
		checkNotFinished();
		// create aspect interface
		Require.requireState(intertypeDeclarations == null, "instanceInterface already generated as: " + intertypeDeclarations);

		AjrsStatement statement = (AjrsStatement) smell.getNode();
		List<AjrsStatement> duplicates = new ArrayList<AjrsStatement>();
		for (AjrsObject<? extends ASTNode> duplicate : smell.getDuplicates()) {
			duplicates.add((AjrsStatement) duplicate);
		}
		// vygenerujeme cez builder

		System.err.println("AspectInterface name: " + interfaceName);
		add("private interface ").add(interfaceName).begin();
		for (AjrsField field : statement.getReadFields()) {
			System.err.println("Interface getter: " + field);
			Type type = field.getPeer().getType();
			addImport(type);
			String methodName = "get" + Character.toUpperCase(field.getName().charAt(0)) + field.getName().substring(1);
			createMethod("public", type.resolveBinding().getName(), null, methodName, "String realName");
			add(';').nl();
		}
		for (AjrsField field : statement.getWriteFields()) {
			System.err.println("Interface setter: " + field);
			Type type = field.getPeer().getType();
			addImport(type);
			String methodName = "set" + Character.toUpperCase(field.getName().charAt(0)) + field.getName().substring(1);
			createMethod("public", "void", null, methodName, "String realName", type.resolveBinding().getName() + " " + field.getName());
			add(';').nl();
		}
		end();

		Set<String> duplicateClasses = new HashSet<String>();
		for (AjrsStatement duplicate : duplicates) {
			ITypeBinding binding = duplicate.getAjrsType().getPeer().resolveBinding();
			String typePackage = binding.getPackage().getName();
			if (typePackage != null && !typePackage.isEmpty()) {
				addImport(typePackage + "." + binding.getQualifiedName());
			}
			duplicateClasses.add(binding.getName());
			duplicate.getReadFields();
		}

		for (String duplicateClass : duplicateClasses) {
			add("declare parents: ").add(duplicateClass).add(" implements ").add(interfaceName).add(';').nl();
			createReadWriteInterfaceImplementation(statement, duplicates, true, false, duplicateClass);
			createReadWriteInterfaceImplementation(statement, duplicates, false, true, duplicateClass);
		}

		intertypeDeclarations = builder.toString();
		System.err.println("Intertype declarations: " + intertypeDeclarations);
		clear();
	}

	public void definePointcutsAndAdvices(AjrsAopDuplicatedCode smell) {
		clear();
		checkNotFinished();
		AjrsStatement statement = (AjrsStatement) smell.getNode();
		List<AjrsStatement> duplicates = new ArrayList<AjrsStatement>();
		for (AjrsObject<? extends ASTNode> duplicate : smell.getDuplicates()) {
			duplicates.add((AjrsStatement) duplicate);
		}
		for (AjrsStatement duplicate : duplicates) {
			clear();
			String adviceType = null;
			if (duplicate.getInputDependencyCount() == 0) {// pouzije sa before execution na metodu
				AjrsMethod method = duplicate.getAjrsMethod();
				MethodDeclaration md = method.getPeer();
				Type type = md.getReturnType2();

				addImport(type);
				ITypeBinding typeBinding = type.resolveBinding();
				addImport(type);
				add("execution( ").add(typeBinding.getName()).space().add(md.getName().getFullyQualifiedName()).add("(..))");
				createStandardPointcutPart(method);
				adviceType = "before";
			}
			String pointcutDef = builder.toString();
			List<String> params = new ArrayList<String>();
			params.add(interfaceName + " objectWithAspectInterface");
			String pointcutName = createPointcut(pointcutDef, params, null);
			createDuplicateAdvice(adviceType, pointcutName, duplicate);
		}
		clear();
		createAdviceImplementation(statement);
	}

	private void createAdviceImplementation(AjrsStatement statement) {
		clear();
		List<String> params = new ArrayList<String>();
		params.add(interfaceName + " objectWithAspectInterface");
		// we have to find real parameter names!
		Set<AjrsField> fields = statement.getFields();
		for (AjrsField field : fields) {
			params.add("String real" + camelCase(field.getName()) + "Name");
		}
		// we have to find real literal names!
		List<Object> literals = statement.getLiterals();
		createMethod("private", "void", null, "adviceImplementation", params.toArray(new String[params.size()]));
		begin();
		add("/*TODO copy node implementation with applied real names*/").nl();
		end();
		methods.add(builder.toString());
		clear();
	}

	private void createStandardPointcutPart(AjrsMethod method) {
		add(" && within(").add(method.getType().getPeer().getName().getFullyQualifiedName()).add(")");
		add(" && this(objectWithAspectInterface)");
	}

	private void createReadWriteInterfaceImplementation(AjrsStatement statement, List<AjrsStatement> duplicates, boolean read, boolean write, String duplicateClass) {
		List<AjrsField> customFields = new ArrayList<AjrsField>();
		if (read) {
			customFields = statement.getReadFields();
		}
		if (write) {
			customFields = statement.getWriteFields();
		}
		for (int i = 0; i < customFields.size(); ++i) {
			AjrsField customField = customFields.get(i);
			if (read) {
				createMethod("public", customField.getPeer().getType().resolveBinding().getName(), duplicateClass, "get" + camelCase(customField.getName()), "String realName");
			}
			if (write) {
				createMethod("public", "void", duplicateClass, "set" + camelCase(customField.getName()), "String realName", customField.getPeer().getType().resolveBinding().getName() + " "
						+ customField.getName());
			}

			begin();
			Set<String> usedFields = new HashSet<String>();
			for (AjrsStatement duplicate : duplicates) {
				AjrsField duplicateField = null;
				if (read) {
					duplicateField = duplicate.getReadFields().get(i);
				}
				if (write) {
					duplicateField = duplicate.getWriteFields().get(i);
				}
				String duplicatedFieldName = duplicateField.getName().trim();
				if (!usedFields.contains(duplicatedFieldName)) {
					usedFields.add(duplicatedFieldName);
					if (read) {
						add("if(realName.equals(").addQuoted(duplicatedFieldName).add("))").begin();
						add("return this.").add(duplicateField.getName()).add(';').nl();
					}
					if (write) {
						add("if(realName.equals(").addQuoted(duplicatedFieldName).add("))").begin();
						add("this.").add(duplicatedFieldName).add(" = ").add(customField.getName()).add(';').nl();
						add("return;").nl();

					}
					end();
				}
			}
			add("throw new AssertionError(").addQuoted("Failed to resolve variable").add("+ realName").add(");").nl();
			end();
		}
	}

	private String camelCase(String name) {
		return Character.toUpperCase(name.charAt(0)) + name.substring(1);
	}

	private void createMethod(String methodType, String returnType, String methodOwner, String methodName, String... params) {
		add(methodType).space().add(returnType).space();
		if (methodOwner != null) {
			add(methodOwner).add('.');
		}
		add(methodName).add('(');
		add(params, ",").add(')');
	}

	private void addImport(String type) {
		checkNotFinished();
		if (type.startsWith("java.")) {
			return;
		}
		imports.add(type);
	}

	private void addImport(Type type) {
		checkNotFinished();
		if (type.isPrimitiveType()) {
			return;
		}
		if (type.isArrayType()) {
			return;
		}
		ITypeBinding binding = type.resolveBinding();
		String typePackage = binding.getPackage().getName();
		if (typePackage == null || typePackage.isEmpty()) {
			return;
		}
		if (typePackage != null && !typePackage.isEmpty()) {
			addImport(typePackage + "." + binding.getQualifiedName());
		}
	}

	public void createAdviceImplemententation(AjrsObject<? extends ASTNode> node) {
		checkNotFinished();
		// TODO Auto-generated method stub

	}

	private AjrsAspectGenerator add(char c) {
		builder.append(c);
		return this;
	}

	private AjrsAspectGenerator add(String string) {
		if (string == null) {
			return this;
		}
		String[] lines = string.split("\n");
		if (lines.length == 1) {
			builder.append(string);
		} else {
			for (String line : lines) {
				line = line.trim();
				builder.append(line);
				nl();
			}
		}

		return this;
	}

	// string construction methods
	public AjrsAspectGenerator begin() {
		return start();
	}

	public AjrsAspectGenerator start() {
		add('{');
		++tabsCounter;
		return nl();
	}

	public AjrsAspectGenerator end() {
		--tabsCounter;
		return add('}').nl();
	}

	public AjrsAspectGenerator nl() {
		builder.append(lineSeparator);
		for (int i = 0; i < tabsCounter; ++i) {
			builder.append("   ");
		}
		return this;
	}

	private AjrsAspectGenerator space() {
		return add(' ');
	}

	private AjrsAspectGenerator addQuoted(String string) {
		add('"');
		add(string);
		return add('"');
	}

	private AjrsAspectGenerator add(Object[] objects, String separator) {
		for (int i = 0; i < objects.length; i++) {
			add(objects[i].toString());
			if (i + 1 < objects.length) {
				add(',');
			}
		}
		return this;
	}

	private void clear() {
		checkNotFinished();
		builder.delete(0, builder.length());
	}

	// totally unimportant methods
	private void checkFinished() {
		Require.requireState(finished, "Generator not finished finished! Cannot continue");
	}

	private void checkNotFinished() {
		Require.requireState(!finished, "Generator already finished! Cannot continue");
	}

}
