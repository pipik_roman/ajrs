package ajrs.wizard;

import java.io.ByteArrayInputStream;
import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jface.wizard.Wizard;

import ajrs.data.AjrsObject;
import ajrs.data.smells.duplicatedCode.AjrsAopDuplicatedCode;
import ajrs.data.statement.AjrsStatement;

public class AjrsAopDuplicatedCodeRefactoringWizard extends Wizard {
	private final AspectInformationWizardPage aspectInformationWizardPage = new AspectInformationWizardPage();
	{
		addPage(aspectInformationWizardPage);
	}

	private final AjrsAopDuplicatedCode smell;

	public AjrsAopDuplicatedCodeRefactoringWizard(AjrsAopDuplicatedCode smell) {
		this.smell = smell;
		// aspectInformationWizardPage.setAspectPackage(smell.getPackageFile());
	}

	@Override
	public boolean performFinish() {
		try {
			IFile aspectFile = getAspectFile();
			AjrsAspectGenerator generator = new AjrsAspectGenerator(aspectInformationWizardPage.getAspectName());
			generator.defineIntertypeDeclarations(smell);
			generator.definePointcutsAndAdvices(smell);
			// generator.createAdviceImplemententation(smell.getNode());
			// String pointcutName = generator.createPointcut("call(* unkown())", pointcutParams, pointcutArgs);
			// generator.createAdvice("before", pointcutName, smell.getNode());
			generator.finish();
			ByteArrayInputStream bais = new ByteArrayInputStream(generator.getBytes());
			aspectFile.create(bais, true, null);
		} catch (JavaModelException e) {
			e.printStackTrace();
		} catch (CoreException e) {
			e.printStackTrace();
		}

		for (AjrsObject<? extends ASTNode> duplicate : smell.getDuplicates()) {
			AjrsStatement statementDuplicate = (AjrsStatement) duplicate;
			System.err.println("TODO process statement duplicate: " + statementDuplicate);
		}
		return true;
	}

	private IFile getAspectFile() throws JavaModelException {
		IResource smellResource = smell.getNode().getAjrsUnit().getPeer().getJavaElement().getCorrespondingResource();
		String packageName = smell.getPackage();
		if (!packageName.endsWith(File.separator)) {
			packageName = packageName + File.separator;
		}
		IFile aspectPath = smellResource.getProject().getFile(packageName + aspectInformationWizardPage.getAspectName() + ".aj");
		System.err.println("Aspect path: " + aspectPath);
		return aspectPath;
	}
}
