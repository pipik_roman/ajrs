package ajrs.wizard;

import java.util.regex.Pattern;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class AspectInformationWizardPage extends WizardPage {

	private final Pattern patternAspectName = Pattern.compile("[a-z_][a-z_0-9]*", Pattern.CASE_INSENSITIVE);
	private Text textAspectName;

	public AspectInformationWizardPage() {
		super("Initial aspect informations");
		setPageComplete(false);
	}

	@Override
	public void createControl(Composite parent) {
		RowLayout layout = new RowLayout(SWT.VERTICAL);
		parent.setLayout(layout);
		setControl(parent);
		Label labelAspectName = new Label(parent, SWT.NONE);
		labelAspectName.setText("Aspect name:");
		textAspectName = new Text(parent, SWT.SINGLE + SWT.BORDER);
		textAspectName.setTextLimit(50);
		// Label labelAspectPackage = new Label(parent, SWT.NONE);
		// labelAspectPackage.setText("Aspect package:");
		// textAspectPackage = new Text(parent, SWT.SINGLE + SWT.BORDER);
		// textAspectPackage.setTextLimit(50);
		ModifyListener ml = new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				setPageComplete(validatePage());
			}
		};
		textAspectName.addModifyListener(ml);
		// textAspectPackage.addModifyListener(ml);
	}

	protected boolean validatePage() {
		if (!patternAspectName.matcher(getAspectName()).matches()) {
			setErrorMessage("Invalid aspect name");
			return false;
		}
		// if (!patternAspectPackage.matcher(getAspectPackage()).matches()) {
		// setErrorMessage("Invalid aspect package");
		// return false;
		// }
		return true;
	}

	public String getAspectName() {
		return textAspectName.getText();
	}

	public String getAspectPackage() {
		return "";// textAspectPackage.getText();
	}

	public void setAspectPackage(String aspectPackage) {
		// textAspectPackage.setText(aspectPackage);
	}
}
