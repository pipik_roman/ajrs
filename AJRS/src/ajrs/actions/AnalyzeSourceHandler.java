package ajrs.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;

import ajrs.builder.AjrsProcessor;

public class AnalyzeSourceHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IEvaluationContext context = (IEvaluationContext) event.getApplicationContext();
		final TreeSelection selection = (TreeSelection) context.getVariable("selection");
		Job job = new Job("Analyze Source") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("AnalyzingSource", selection.size());
				for (TreePath path : selection.getPaths()) {
					ICompilationUnit compilationUnit = (ICompilationUnit) path.getLastSegment();
					monitor.subTask("Analyzing source of " + compilationUnit.getElementName());
					AjrsProcessor processor = new AjrsProcessor(compilationUnit);
					processor.parse(monitor);
				}
				return Status.OK_STATUS;
			}
		};
		job.schedule();
		return null;
	}
}
