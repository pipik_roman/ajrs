package ajrs.actions;

import org.eclipse.core.commands.State;
import org.eclipse.jface.commands.ToggleState;

public class ToggleShowDuplicationDataState extends ToggleState {
	public static final String ID = "AJRS.ToggleShowDuplicationDataState";

	public static State getState() {
		return ToggleShowDuplicationData.getCommand().getState(ID);
	}

	public static boolean getStateValue() {
		return (Boolean) getState().getValue();
	}
}
