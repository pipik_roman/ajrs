package ajrs.actions;

import org.eclipse.core.commands.State;
import org.eclipse.jface.commands.ToggleState;

public class ToggleShowHashState extends ToggleState {
	public static final String ID = "AJRS.ToggleShowHashState";

	public static State getState() {
		return ToggleShowHash.getCommand().getState(ID);
	}

	public static boolean getStateValue() {
		return (Boolean) getState().getValue();
	}
}
