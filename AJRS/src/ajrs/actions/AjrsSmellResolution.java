package ajrs.actions;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;

import ajrs.data.smells.AjrsSmellCandidate;
import ajrs.data.smells.duplicatedCode.AjrsAopDuplicatedCode;
import ajrs.wizard.AjrsAopDuplicatedCodeRefactoringWizard;

public class AjrsSmellResolution {
	AjrsSmellCandidate candidate;

	public AjrsSmellResolution(AjrsSmellCandidate candidate) {
		this.candidate = candidate;
	}

	public void resolve() {
		System.err.println("AjrsSmellResolution.resolve()");
		Wizard wizard = null;
		if (candidate instanceof AjrsAopDuplicatedCode) {
			wizard = new AjrsAopDuplicatedCodeRefactoringWizard((AjrsAopDuplicatedCode) candidate);
		}

		WizardDialog dialog = new WizardDialog(null, wizard);
		dialog.create();
		dialog.open();
	}
}
