package ajrs.actions;

import java.io.InputStream;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IMarkerResolution;
import org.eclipse.ui.IMarkerResolutionGenerator;
import org.eclipse.ui.views.markers.WorkbenchMarkerResolution;

import ajrs.builder.AjrsBuilder;
import ajrs.data.smells.AjrsSmellCandidate;

public class AjrsMarkerResolution extends WorkbenchMarkerResolution implements IMarkerResolutionGenerator {

	@Override
	public String getDescription() {
		return "Starts wizard to extract smell / aspect";
	}

	@Override
	public Image getImage() {
		Display display = Display.getDefault();
		if (display == null) {
			throw new AssertionError("Failed to obtain display");
		}
		InputStream stream = this.getClass().getResourceAsStream("wizard.png");
		if (stream == null) {
			throw new AssertionError("Failed to obtain stream");
		}
		return new Image(display, stream);
	}

	@Override
	public String getLabel() {
		return "Extract smell / aspect";
	}

	@Override
	public void run(IMarker marker) {
		AjrsSmellCandidate candidate = AjrsBuilder.markerToSmell.get(marker);
		if (candidate == null) {
			throw new IllegalArgumentException("Marker resolution failed. No candidate for marker " + marker);
		}

		System.err.println("Resoluting smell candidate:" + candidate);
		new AjrsSmellResolution(candidate).resolve();
	}

	@Override
	public IMarker[] findOtherMarkers(IMarker[] markers) {
		return null;
	}

	@Override
	public IMarkerResolution[] getResolutions(IMarker marker) {
		int severity;
		try {
			severity = (Integer) marker.getAttribute(IMarker.SEVERITY);
		} catch (CoreException e) {
			e.printStackTrace();
			return new IMarkerResolution[] {};
		}
		if (severity != IMarker.SEVERITY_WARNING) {
			return new IMarkerResolution[] {};// we can resolve only warnings
		}
		return new IMarkerResolution[] { new AjrsMarkerResolution() };
	}
}
