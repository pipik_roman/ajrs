package ajrs.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.State;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;

import ajrs.Activator;
import ajrs.preferences.PreferenceConstants;

public class ToggleShowDuplicationData extends AbstractHandler {
	public static final String ID = "AJRS.ToggleShowDuplicationData";

	public static Command getCommand() {
		ICommandService service = (ICommandService) PlatformUI.getWorkbench().getService(ICommandService.class);
		return service.getCommand(ID);
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		State state = ToggleShowDuplicationDataState.getState();
		boolean stateValue = !(Boolean) state.getValue();
		state.setValue(stateValue);
		// set value in preferences!
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setValue(PreferenceConstants.GENERATE_STATEMENT_HASH_MARK, stateValue);
		return null;
	}
}
