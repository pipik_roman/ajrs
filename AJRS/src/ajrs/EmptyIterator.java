/**
 * 
 */
package ajrs;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Empty iterator
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <T>
 *            type of elements
 */
public class EmptyIterator<T> implements Iterator<T> {

	@Override
	public boolean hasNext() {
		return false;
	}

	@Override
	public T next() {
		throw new NoSuchElementException();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
