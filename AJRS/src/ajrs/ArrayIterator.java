/**
 * 
 */
package ajrs;

import java.util.Iterator;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <Element>
 *            array elements
 */
public class ArrayIterator<Element> implements Iterator<Element> {

	private final Element[] elements;
	int index = 0;

	public ArrayIterator(Element[] elements) {
		assert elements != null;
		this.elements = elements;
	}

	@Override
	public boolean hasNext() {
		return index < elements.length;
	}

	@Override
	public Element next() {
		return elements[index++];
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
