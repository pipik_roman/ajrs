/**
 * 
 */
package ajrs.data.statement;

import java.util.List;

import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;

import ajrs.Activator;
import ajrs.data.AjrsObjectVisitor;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsTryStatement extends AjrsNonLeafStatement {

	@SuppressWarnings("unchecked")
	public AjrsTryStatement(AjrsStatement ajrsStatement, TryStatement node) {
		super(ajrsStatement, node, TRY_STATEMENT);
		setDescription("try(" + node.resources() + "){} catch " + node.catchClauses() + ", finally " + node.getFinally());

		List<VariableDeclarationExpression> resources = node.resources();
		if (resources != null) {
			for (VariableDeclarationExpression e : resources) {
				e.accept(this);
			}
		}
		nodeDataToHash();
		// TODO vyriesit CatchClause
		dependencies.goInto();
		node.getBody().accept(this);
		dependencies.goOut();

		Block finallyBlock = node.getFinally();
		if (finallyBlock != null) {
			finallyBlock.accept(this);
		}
	}

	@Override
	public boolean hasExpression() {
		return true;
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		boolean children = visitor.visit(this);
		if (children) {
			visitChildren(visitor);
		}
		visitor.afterVisit(this);
	}

	@Override
	public int getComplexityCoefficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.COMPLEXITY_TRY);
	}
}
