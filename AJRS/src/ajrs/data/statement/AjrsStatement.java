/**
 * 
 */
package ajrs.data.statement;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import ajrs.Activator;
import ajrs.AjrsUtils;
import ajrs.actions.ToggleShowHashState;
import ajrs.data.AjrsHash;
import ajrs.data.AjrsMethod;
import ajrs.data.AjrsNode;
import ajrs.data.AjrsNodeData;
import ajrs.data.AjrsObject;
import ajrs.data.AjrsStatementDependencies;
import ajrs.data.AjrsType;
import ajrs.preferences.PreferenceConstants;

/**
 * Reprezentuje akykolvek pouzivany statement. Je to rekurzivny objekt!
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public abstract class AjrsStatement extends AjrsNode<Statement> {
	protected static final int PART_INPUT_STATEMENT_DEPENDENCY = 2;
	protected static final int PART_OUTPUT_STATEMENT_DEPENDENCY = 3;
	public static final int PART_INPUT_VARIABLE_DEPENDENCY = 4;
	public static final int PART_OUTPUT_VARIABLE_DEPENDENCY = 5;
	/**
	 * BODY je 0 aby to neovplyvnovalo hash, pretoze cesta BODY1-> BODY2->EXP-> by mala mat rovnaky hash ako BODY1->EXP (nezáleží na vnorení do bloku)
	 */
	public static final int BODY = "BODY".hashCode();
	public static final int RETURN_STATEMENT = "RETURN".hashCode();
	public static final int CONDITIONAL_STATEMENT = "CONDITIONAL".hashCode();
	public static final int LOOP_STATEMENT = "LOOP".hashCode();
	public static final int THROW_STATEMENT = "THROW".hashCode();
	public static final int TRY_STATEMENT = "TRY".hashCode();
	public static final int VARIABLE_STATEMENT = "VARIABLE".hashCode();
	public static final int EXPRESSION_STATEMENT = "EXPRESSION".hashCode();
	public static final int BREAK_STATEMENT = "BREAK".hashCode();
	public static final int CONTINUE_STATEMENT = "CONTINUE".hashCode();
	/**
	 * Je to static, pretoze sa naraz moze prechadzat len cez jeden statement, a kazdy non leaf statement musi volat goInto a goOut
	 */
	protected static final AjrsStatementDependencies dependencies = new AjrsStatementDependencies();

	/**
	 * @param parent
	 *            parent tohto statementu
	 * @param statement
	 *            statement
	 * @param statementType
	 *            type of statement
	 */
	public AjrsStatement(AjrsObject<?> parent, Statement statement, int statementType) {
		super(4, 6, parent, statement, true);
		setHash(0, new int[] { statementType });
	}

	@Override
	protected void nodeDataToHash() {
		AjrsNodeData nodeData = getNodeData();
		int[] parameters = AjrsUtils.asIntArray(nodeData.getParameters());
		int[] inVarDep = AjrsUtils.asIntArray(nodeData.getInputDependencies());
		int[] outVarDep = AjrsUtils.asIntArray(nodeData.getOutputDependencies());
		int[] inStatDep;
		int[] outStatDep = new int[0];// zatial sa neriesi

		List<Integer> inputStatementDependencies = dependencies.getStatementDependencyHashes(this, inVarDep);

		// vzdy je zavislost na parenta s expression ak taky existuje
		AjrsStatement parentWithExpression = getParentWithExpression();
		if (parentWithExpression != null) {
			inputStatementDependencies.add(parentWithExpression.hashCode());
		}

		inStatDep = AjrsUtils.asIntArray(inputStatementDependencies);

		setHash(PART_PARAMETERS, parameters);
		setHash(PART_INPUT_VARIABLE_DEPENDENCY, inVarDep);
		setHash(PART_OUTPUT_VARIABLE_DEPENDENCY, outVarDep);
		setHash(PART_INPUT_STATEMENT_DEPENDENCY, inStatDep);
		setHash(PART_OUTPUT_STATEMENT_DEPENDENCY, outStatDep);

		// teraz aktualizujeme dependencies na tento statement
		dependencies.addOutputVariableDependencies(this);

		if (ToggleShowHashState.getStateValue()) {// ak sa maju zobrazovat debug vypisy
			addMarker(getHash().toString(), getPeer().getStartPosition(), getPeer().getLength(), IMarker.SEVERITY_INFO, true);
		}
	}

	private AjrsStatement getParentWithExpression() {
		if (this instanceof AjrsMethodBlock) {
			return null;
		}
		AjrsNonLeafStatement parent = (AjrsNonLeafStatement) getParent();
		while (!(parent instanceof AjrsMethodBlock)) {
			if (parent.hasExpression()) {
				return parent;
			} else {
				parent = (AjrsNonLeafStatement) parent.getParent();
			}
		}
		return null;
	}

	public abstract boolean isLeaf();

	abstract public int getComplexityCoefficient();

	public int getComplexity() {
		int[] inputs = getHash(AjrsStatement.PART_INPUT_VARIABLE_DEPENDENCY);
		int[] outputs = getHash(AjrsStatement.PART_OUTPUT_VARIABLE_DEPENDENCY);
		assert inputs != null : "Every statement should have resolved dependency now, but: " + getClass() + ", " + this + " has not";
		assert outputs != null : "Every statement should have resolved dependency now, but: " + getClass() + ", " + this + " has not";
		int complexityCoefficient = getComplexityCoefficient();
		return (complexityCoefficient) + (complexityCoefficient * ((inputs.length * getInputVariableCoefficient()) + outputs.length * getOutputVariableCoefficient()));
	}

	public int getInputVariableCoefficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.INPUT_VARIABLE_COEF);
	}

	public int getOutputVariableCoefficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.OUTPUT_VARIABLE_COEF);
	}

	@Override
	public boolean visit(VariableDeclarationFragment node) {
		Expression initializer = node.getInitializer();
		if (initializer != null) {
			state.setAssignmentRight(true);
			initializer.accept(this);
			state.setAssignmentRight(false);
		}

		state.setAssignmentLeft(true);
		node.getName().accept(this);
		state.setAssignmentLeft(false);

		return false;
	}

	@Override
	protected void processMethodBinding(IMethodBinding method) {
		if (method.isDefaultConstructor()) {
			return;
		}
		if (isInThisType(method)) {
			AjrsHash hash = AjrsMethod.createHash(method);
			AjrsMethod ajrsMethod = getType().getMethods().get(hash);
			AjrsNodeData nodeData = getNodeData();
			assert ajrsMethod != null : "Ajrs Method should not be null! Method: " + method + ", methods: " + getType().getMethods();
			nodeData.addInputDependency(ajrsMethod.getInputDependency());
			nodeData.addOutputDependency(ajrsMethod.getOutputDependency());
		}
		AjrsNodeData nodeData = getNodeData();
		int hash = method.getName().hashCode() + METHOD_INVOCATION_HASH_OFFSET;
		nodeData.addParameter(hash);
	}

	@Override
	protected void processLocal(IVariableBinding variable, boolean isDeclaration) {
		addDependency(variable, true, false);
	}

	@Override
	protected void processParameter(IVariableBinding variable, boolean isDeclaration) {
		addDependency(variable, false, true);
	}

	/*--------------------------------------------- SPRACOVANIE STATEMENTOV-------------------------------------*/

	@Override
	public void addDependency(IVariableBinding variable, boolean local, boolean parameter) {
		super.addDependency(variable, local, parameter);
		if (!state.isLeft() || state.isRight()) {
			int integer = variable.getType().getQualifiedName().hashCode() + (local ? LOCAL_VARIABLE_HASH_OFFSET : 0);
			getNodeData().addParameter(integer);
		}
	}

	@Override
	public AjrsType getType() {
		return getParent().getType();
	}

	@Override
	public AjrsNode<? extends ASTNode> getParent() {
		return (AjrsNode<? extends ASTNode>) super.getParent();
	}

	/**
	 * Vrati vsetkych parentov tohto statementu az pokial sa nenajde spolocny parent. Priamy parent tohto statementu je prvy a spolocny parent je posledny
	 * 
	 * @param dependencyStatement
	 *            statement ktory ma spolocneho parenta s tymto statementom
	 * @return vsetkych parentov tohto statementu az pokial sa nenajde spolocny parent. Priamy parent tohto statementu je prvy a spolocny parent je posledny
	 * @throws IllegalArgumentException
	 *             ak nemaju spolocneho parenta tato metoda by nemala byt volana!!!! Tiez by nemala byt volana pre AjrsMethodBlock, pretoze jeho parent nieje {@link AjrsStatement}
	 */
	public List<AjrsNonLeafStatement> getPathToParent(AjrsStatement dependencyStatement) throws IllegalArgumentException {
		try {
			List<AjrsNonLeafStatement> parents = new ArrayList<AjrsNonLeafStatement>();
			AjrsNonLeafStatement parent = (AjrsNonLeafStatement) getParent();
			assert parent != null;
			while (parent != dependencyStatement.getParent()) {
				parents.add(parent);
				parent = (AjrsNonLeafStatement) parent.getParent();
			}
			parents.add(parent);
			return parents;
		} catch (ClassCastException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public boolean isAopRefactoringPossible() {
		return localReferences == 0;
	}

	public int getInputDependencyCount() {
		return getHash(PART_INPUT_STATEMENT_DEPENDENCY).length;
	}

	public int getOutputDependencyCount() {
		return getHash(PART_OUTPUT_STATEMENT_DEPENDENCY).length;
	}
	/*------------------------------------------------SPRACOVANIE EXPRESSIONS--------------------------------------------*/
	// infix expression je generic

}
