/**
 * 
 */
package ajrs.data.statement;

import org.eclipse.jdt.core.dom.ThrowStatement;

import ajrs.Activator;
import ajrs.data.AjrsObjectVisitor;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsThrowStatement extends AjrsLeafStatement {

	public AjrsThrowStatement(AjrsNonLeafStatement ajrsStatement, ThrowStatement node) {
		super(ajrsStatement, node, THROW_STATEMENT);
		setDescription("throw " + node.getExpression());
		node.getExpression().accept(this);
		nodeDataToHash();
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int getComplexityCoefficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.COMPLEXITY_THROW);
	}
}
