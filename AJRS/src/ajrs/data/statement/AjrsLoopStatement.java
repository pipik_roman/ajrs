/**
 * 
 */
package ajrs.data.statement;

import java.util.List;

import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.WhileStatement;

import ajrs.Activator;
import ajrs.data.AjrsObject;
import ajrs.data.AjrsObjectVisitor;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsLoopStatement extends AjrsNonLeafStatement {

	public AjrsLoopStatement(AjrsObject<?> parent, DoStatement statement) {
		super(parent, statement, LOOP_STATEMENT);
		setDescription("do{} while(" + statement.getExpression() + ")");

		Statement body = statement.getBody();
		body.accept(this);
		statement.getExpression().accept(this);
		nodeDataToHash();
		// nevola sa dependencies.goInto lebo sa to aj tak musi raz vykonat takze je to ako keby to bolo v tele. Vsetko sa vykona zaradom
	}

	public AjrsLoopStatement(AjrsStatement ajrsStatement, WhileStatement node) {
		super(ajrsStatement, node, LOOP_STATEMENT);
		setDescription("while(" + node.getExpression() + "){}");
		node.getExpression().accept(this);
		nodeDataToHash();
		dependencies.goInto();
		node.getBody().accept(this);
		dependencies.goOut();
	}

	@SuppressWarnings("unchecked")
	public AjrsLoopStatement(AjrsStatement ajrsStatement, ForStatement node) {
		super(ajrsStatement, node, LOOP_STATEMENT);
		setDescription("for(" + node.initializers() + ";" + node.getExpression() + ";" + node.updaters() + "){}");
		List<Expression> initializers = node.initializers();
		if (initializers != null) {
			for (Expression initializer : initializers) {
				initializer.accept(this);
			}
		}
		node.getExpression().accept(this);
		List<Expression> updaters = node.updaters();
		if (updaters != null) {
			for (Expression updater : updaters) {
				updater.accept(this);
			}
		}
		nodeDataToHash();
		dependencies.goInto();
		node.getBody().accept(this);
		dependencies.goOut();
	}

	public AjrsLoopStatement(AjrsStatement ajrsStatement, EnhancedForStatement node) {
		super(ajrsStatement, node, LOOP_STATEMENT);
		setDescription("for(" + node.getExpression() + "){}");
		node.getParameter().accept(this);
		node.getExpression().accept(this);
		nodeDataToHash();
		dependencies.goInto();
		node.getBody().accept(this);
		dependencies.goOut();
	}

	@Override
	public boolean hasExpression() {
		return true;
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		boolean children = visitor.visit(this);
		if (children) {
			visitChildren(visitor);
		}
		visitor.afterVisit(this);
	}

	@Override
	public int getComplexityCoefficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.COMPLEXITY_LOOP);
	}
}
