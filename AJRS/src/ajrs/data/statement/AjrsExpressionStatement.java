/**
 * 
 */
package ajrs.data.statement;

import org.eclipse.jdt.core.dom.ExpressionStatement;

import ajrs.Activator;
import ajrs.data.AjrsObjectVisitor;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsExpressionStatement extends AjrsLeafStatement {

	/**
	 * @param parent
	 *            parent statement or method
	 * @param statement
	 *            this statement
	 */
	public AjrsExpressionStatement(AjrsNonLeafStatement parent, ExpressionStatement statement) {
		super(parent, statement, EXPRESSION_STATEMENT);
		setDescription(statement.toString());
		statement.getExpression().accept(this);
		nodeDataToHash();
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int getComplexityCoefficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.COMPLEXITY_EXPRESSION);
	}
}
