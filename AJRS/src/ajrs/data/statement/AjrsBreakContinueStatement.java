/**
 * 
 */
package ajrs.data.statement;

import org.eclipse.jdt.core.dom.BreakStatement;
import org.eclipse.jdt.core.dom.ContinueStatement;

import ajrs.Activator;
import ajrs.data.AjrsObjectVisitor;
import ajrs.preferences.PreferenceConstants;

/**
 * Break or Continue statement
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsBreakContinueStatement extends AjrsLeafStatement {

	public AjrsBreakContinueStatement(AjrsNonLeafStatement parent, BreakStatement statement) {
		super(parent, statement, BREAK_STATEMENT);
		nodeDataToHash();
		setDescription("break");
	}

	public AjrsBreakContinueStatement(AjrsNonLeafStatement parent, ContinueStatement statement) {
		super(parent, statement, CONTINUE_STATEMENT);
		nodeDataToHash();
		setDescription("continue");
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int getComplexityCoefficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.COMPLEXITY_BREAK_CONTINUE);
	}
}
