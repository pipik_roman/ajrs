/**
 * 
 */
package ajrs.data.statement;

import org.eclipse.jdt.core.dom.Statement;

/**
 * Takyto statement nema dalsie vnorene statementy
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public abstract class AjrsLeafStatement extends AjrsStatement {

	public AjrsLeafStatement(AjrsNonLeafStatement parent, Statement statement, int statementType) {
		super(parent, statement, statementType);
	}

	@Override
	public boolean isLeaf() {
		return true;
	}

}
