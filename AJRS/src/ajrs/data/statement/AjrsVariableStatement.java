/**
 * 
 */
package ajrs.data.statement;

import java.util.List;

import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

import ajrs.Activator;
import ajrs.data.AjrsObjectVisitor;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsVariableStatement extends AjrsLeafStatement {

	@SuppressWarnings("unchecked")
	public AjrsVariableStatement(AjrsNonLeafStatement parent, VariableDeclarationStatement statement) {
		super(parent, statement, VARIABLE_STATEMENT);
		setDescription(statement.toString());
		List<VariableDeclarationFragment> fragments = statement.fragments();
		for (VariableDeclarationFragment fragment : fragments) {
			fragment.accept(this);
		}
		nodeDataToHash();
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int getComplexityCoefficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.COMPLEXITY_VARIABLE);
	}
}
