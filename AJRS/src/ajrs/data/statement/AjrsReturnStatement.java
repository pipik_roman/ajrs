/**
 * 
 */
package ajrs.data.statement;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ReturnStatement;

import ajrs.Activator;
import ajrs.data.AjrsObjectVisitor;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsReturnStatement extends AjrsLeafStatement {

	public AjrsReturnStatement(AjrsNonLeafStatement ajrsStatement, ReturnStatement node) {
		super(ajrsStatement, node, RETURN_STATEMENT);
		setDescription("return " + node.getExpression());
		Expression e = node.getExpression();
		if (e != null) {
			e.accept(this);
		}
		nodeDataToHash();
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int getComplexityCoefficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.COMPLEXITY_RETURN);
	}
}
