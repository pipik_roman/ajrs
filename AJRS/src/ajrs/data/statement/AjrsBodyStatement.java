/**
 * 
 */
package ajrs.data.statement;

import org.eclipse.jdt.core.dom.Statement;

import ajrs.Activator;
import ajrs.data.AjrsObjectVisitor;
import ajrs.preferences.PreferenceConstants;

/**
 * Obaluje body, napriklad pre if potrebujeme obalit body
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsBodyStatement extends AjrsNonLeafStatement {

	public AjrsBodyStatement(AjrsStatement parent, Statement statement) {
		super(parent, statement, BODY);
		setDescription("");
		nodeDataToHash();
		statement.accept(this);
		// nevolame dependencies.goInto() lebo pre if a podobne sa to vola pred tymto, a pri cistom block statement by to len sposobilo odstranenie dependencies ktore v skutocnosti su
	}

	@Override
	protected void nodeDataToHash() {
		int[] parameters = new int[0];
		int[] inVarDep = new int[0];
		int[] outVarDep = new int[0];
		int[] inStatDep = new int[0];
		int[] outStatDep = new int[0];// zatial sa neriesi
		setHash(PART_PARAMETERS, parameters);
		setHash(PART_INPUT_VARIABLE_DEPENDENCY, inVarDep);
		setHash(PART_OUTPUT_VARIABLE_DEPENDENCY, outVarDep);
		setHash(PART_INPUT_STATEMENT_DEPENDENCY, inStatDep);
		setHash(PART_OUTPUT_STATEMENT_DEPENDENCY, outStatDep);
	}

	@Override
	public boolean hasExpression() {
		return false;
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		boolean children = visitor.visit(this);
		if (children) {
			visitChildren(visitor);
		}
		visitor.afterVisit(this);
	}

	@Override
	public int getComplexityCoefficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.COMPLEXITY_BODY);
	}
}
