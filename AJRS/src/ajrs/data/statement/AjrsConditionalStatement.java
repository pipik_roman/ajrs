/**
 * 
 */
package ajrs.data.statement;

import java.util.List;

import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SwitchStatement;

import ajrs.Activator;
import ajrs.data.AjrsObjectVisitor;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsConditionalStatement extends AjrsNonLeafStatement {

	@SuppressWarnings("unchecked")
	public AjrsConditionalStatement(AjrsStatement ajrsStatement, SwitchStatement node) {
		super(ajrsStatement, node, CONDITIONAL_STATEMENT);
		setDescription("switch (" + node.getExpression() + ")");
		node.getExpression().accept(this);
		nodeDataToHash();
		List<Statement> statements = node.statements();
		if (statements != null) {
			for (Statement s : statements) {
				// TODO osetrit statementy nemaju medzi sebou zavislosti
				s.accept(this);
			}
		}
	}

	@Override
	public boolean hasExpression() {
		return true;
	}

	public AjrsConditionalStatement(AjrsStatement ajrsStatement, IfStatement node) {
		super(ajrsStatement, node, CONDITIONAL_STATEMENT);
		setDescription("if()" + node.getExpression() + "{}");
		node.getExpression().accept(this);
		nodeDataToHash();

		Statement thenStatement = node.getThenStatement();
		if (thenStatement != null) {
			dependencies.goInto();
			thenStatement.accept(this);
			dependencies.goOut();
		}

		Statement elseStatement = node.getElseStatement();
		if (elseStatement != null) {
			dependencies.goInto();
			elseStatement.accept(this);
			dependencies.goOut();
		}
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		boolean children = visitor.visit(this);
		if (children) {
			visitChildren(visitor);
		}
		visitor.afterVisit(this);
	}

	@Override
	public int getComplexityCoefficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.COMPLEXITY_CONDITIONAL);
	}
}
