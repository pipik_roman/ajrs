/**
 * 
 */
package ajrs.data.statement;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AssertStatement;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.BreakStatement;
import org.eclipse.jdt.core.dom.ContinueStatement;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EmptyStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.LabeledStatement;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.ThrowStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.TypeDeclarationStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;

import ajrs.data.AjrsField;
import ajrs.data.AjrsObject;
import ajrs.data.AjrsObjectVisitor;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
@SuppressWarnings("unchecked")
public abstract class AjrsNonLeafStatement extends AjrsStatement {

	protected final List<AjrsStatement> statements = new ArrayList<AjrsStatement>();

	{
		setSubObjects(statements);
	}

	public AjrsNonLeafStatement(AjrsObject<?> parent, Statement statement, int statementType) {
		super(parent, statement, statementType);
	}

	public abstract boolean hasExpression();

	public List<AjrsStatement> getStatements() {
		return statements;
	}

	@Override
	public boolean isLeaf() {
		return statements.isEmpty();
	}

	@Override
	public boolean visit(AssertStatement node) {// ignorovanie statementu
		return false;
	}

	@Override
	public boolean visit(BreakStatement node) {
		AjrsStatement statement = new AjrsBreakContinueStatement(this, node);
		statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(ContinueStatement node) {
		AjrsStatement statement = new AjrsBreakContinueStatement(this, node);
		statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(Block node) {
		if (getPeer() == node) {
			return true;
		}// chceme potomkov, nie peer
		AjrsBodyStatement statement = new AjrsBodyStatement(this, node);
		statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(DoStatement node) {
		if (getPeer() == node) {
			return true;
		}// chceme potomkov, nie peer
		AjrsStatement statement = new AjrsLoopStatement(this, node);
		statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(EmptyStatement node) {
		return false;
	}

	@Override
	public boolean visit(ExpressionStatement node) {
		if (getPeer() == node) {
			return true;
		}// chceme potomkov, nie peer
		AjrsStatement statement = new AjrsExpressionStatement(this, node);
		statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(ForStatement node) {
		if (getPeer() == node) {
			return true;
		}// chceme potomkov, nie peer
		AjrsStatement statement = new AjrsLoopStatement(this, node);
		statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(IfStatement node) {
		if (getPeer() == node) {
			return true;
		}// chceme potomkov, nie peer
		AjrsStatement statement = new AjrsConditionalStatement(this, node);
		statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(LabeledStatement node) {
		return true;
	}

	@Override
	public boolean visit(ReturnStatement node) {
		if (getPeer() == node) {
			return true;
		}// chceme potomkov, nie peer
		AjrsStatement statement = new AjrsReturnStatement(this, node);
		statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(SuperConstructorInvocation node) {
		return false;
	}

	@Override
	public boolean visit(SwitchStatement node) {
		if (getPeer() == node) {
			return true;
		}// chceme potomkov, nie peer
		AjrsStatement statement = new AjrsConditionalStatement(this, node);
		statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(SwitchCase node) {
		Expression expression = node.getExpression();
		if (expression != null) {
			expression.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(ThrowStatement node) {
		if (getPeer() == node) {
			return true;
		}// chceme potomkov, nie peer
		AjrsStatement statement = new AjrsThrowStatement(this, node);
		statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(TryStatement node) {
		if (getPeer() == node) {
			return true;
		}// chceme potomkov, nie peer
		AjrsStatement statement = new AjrsTryStatement(this, node);
		statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(TypeDeclarationStatement node) {
		throw new AssertionError("TODO Type declaration statement: " + node);
	}

	@Override
	public boolean visit(VariableDeclarationStatement node) {
		AjrsStatement statement = new AjrsVariableStatement(this, node);
		this.statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(WhileStatement node) {
		AjrsStatement statement = new AjrsLoopStatement(this, node);
		statements.add(statement);
		return false;
	}

	@Override
	public boolean visit(EnhancedForStatement node) {
		AjrsStatement statement = new AjrsLoopStatement(this, node);
		statements.add(statement);
		return false;
	}

	protected void visitChildren(AjrsObjectVisitor visitor) {
		if (statements != null) {
			for (AjrsStatement statement : statements) {
				statement.accept(visitor);
			}
		}
	}

	@Override
	public List<AjrsField> getReadFields() {
		List<AjrsField> list = new ArrayList<AjrsField>(super.getReadFields());
		for (AjrsStatement child : statements) {
			list.addAll(child.getReadFields());
		}
		return list;
	}

	@Override
	public List<AjrsField> getWriteFields() {
		List<AjrsField> list = new ArrayList<AjrsField>(super.getWriteFields());
		for (AjrsStatement child : statements) {
			list.addAll(child.getWriteFields());
		}
		return list;
	}
}
