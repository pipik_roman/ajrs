/**
 * 
 */
package ajrs.data.statement;

import java.util.Iterator;

import org.eclipse.jdt.core.dom.Block;

import ajrs.Activator;
import ajrs.data.AjrsMethod;
import ajrs.data.AjrsObject;
import ajrs.data.AjrsObjectVisitor;
import ajrs.data.AjrsStatementDependencies;
import ajrs.preferences.PreferenceConstants;

/**
 * Zabranuje duplikacii pri spracovani statementov. Vytvara prazdne {@link AjrsStatementDependencies}
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsMethodBlock extends AjrsNonLeafStatement {

	/**
	 * Create statement representing method body
	 * 
	 * @param parent
	 *            parent method
	 * @param body
	 *            body of method
	 */
	public AjrsMethodBlock(AjrsMethod parent, Block body) {
		super(parent, body, AjrsStatement.BODY);
		setDescription("");
		nodeDataToHash();
		// tu nedavame dependencies.goInto lebo vo visitPeer volame dependencies.clear()
	}

	@Override
	public boolean hasExpression() {
		return false;
	}

	@Override
	public AjrsMethod getParent() {
		return (AjrsMethod) super.getParent();
	}

	@Override
	public void print() {
		Iterator<AjrsObject<?>> iterator = this.iterator();
		while (iterator.hasNext()) {
			iterator.next().print();
			System.out.println();
		}
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		boolean children = visitor.visit(this);
		if (children) {
			visitChildren(visitor);
		}
		visitor.afterVisit(this);
	}

	@Override
	public int getComplexityCoefficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.COMPLEXITY_BODY);
	}

	@Override
	public void visitPeer() {
		dependencies.clear();
		super.visitPeer();
	}
}
