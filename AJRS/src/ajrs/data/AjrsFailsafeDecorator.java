/**
 * 
 */
package ajrs.data;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.AnnotationTypeDeclaration;
import org.eclipse.jdt.core.dom.AnnotationTypeMemberDeclaration;
import org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.ArrayInitializer;
import org.eclipse.jdt.core.dom.ArrayType;
import org.eclipse.jdt.core.dom.AssertStatement;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.BlockComment;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.BreakStatement;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.ContinueStatement;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EmptyStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.ImportDeclaration;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.Initializer;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.LabeledStatement;
import org.eclipse.jdt.core.dom.LineComment;
import org.eclipse.jdt.core.dom.MarkerAnnotation;
import org.eclipse.jdt.core.dom.MemberRef;
import org.eclipse.jdt.core.dom.MemberValuePair;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.MethodRef;
import org.eclipse.jdt.core.dom.MethodRefParameter;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.NormalAnnotation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.ParameterizedType;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.PrimitiveType;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.QualifiedType;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.SingleMemberAnnotation;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.SuperFieldAccess;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.SynchronizedStatement;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;
import org.eclipse.jdt.core.dom.ThisExpression;
import org.eclipse.jdt.core.dom.ThrowStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclarationStatement;
import org.eclipse.jdt.core.dom.TypeLiteral;
import org.eclipse.jdt.core.dom.TypeParameter;
import org.eclipse.jdt.core.dom.UnionType;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.eclipse.jdt.core.dom.WildcardType;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <AstPeer>
 *            ast peer object
 */
public abstract class AjrsFailsafeDecorator<AstPeer extends ASTNode> extends AjrsObject<AstPeer> {

	/**
	 * @param uniqueParts
	 *            pocet casti hash ktore tvoria jeho identitu
	 * @param hashParts
	 *            number of hash parts
	 * @param parent
	 *            parent of this object, or null
	 * @param peer
	 *            peer object in ast
	 */
	public AjrsFailsafeDecorator(int uniqueParts, int hashParts, AjrsObject<? extends ASTNode> parent, AstPeer peer) {
		super(uniqueParts, hashParts, parent, peer);
	}

	/*------------------------------------------IMPLEMENTACIE KTORE ZABEZPECUJU ZE SA NA NIC NEZABUDLO-------------------------------*/

	@Override
	public boolean visit(InfixExpression node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(SimpleName node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(AnnotationTypeDeclaration node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(AnnotationTypeMemberDeclaration node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(AnonymousClassDeclaration node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ArrayAccess node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ArrayCreation node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ArrayInitializer node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ArrayType node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(AssertStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(Assignment node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(Block node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(BlockComment node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(BooleanLiteral node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(BreakStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(CastExpression node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(CatchClause node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(CharacterLiteral node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ClassInstanceCreation node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(CompilationUnit node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ConditionalExpression node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ConstructorInvocation node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ContinueStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(DoStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(EmptyStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(EnhancedForStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(EnumConstantDeclaration node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(EnumDeclaration node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ExpressionStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(FieldAccess node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(FieldDeclaration node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ForStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(IfStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ImportDeclaration node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(Initializer node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(InstanceofExpression node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(Javadoc node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(LabeledStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(LineComment node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(MarkerAnnotation node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(MemberRef node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(MemberValuePair node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(MethodDeclaration node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(MethodInvocation node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(MethodRef node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(MethodRefParameter node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(Modifier node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(NormalAnnotation node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(NullLiteral node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(NumberLiteral node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(PackageDeclaration node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ParameterizedType node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ParenthesizedExpression node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(PostfixExpression node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(PrefixExpression node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(PrimitiveType node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(QualifiedName node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(QualifiedType node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ReturnStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(SimpleType node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(SingleMemberAnnotation node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(SingleVariableDeclaration node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(StringLiteral node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(SuperConstructorInvocation node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(SuperFieldAccess node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(SuperMethodInvocation node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(SwitchCase node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(SwitchStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(SynchronizedStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(TagElement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(TextElement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ThisExpression node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(ThrowStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(TryStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(TypeDeclaration node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(TypeDeclarationStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(TypeLiteral node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(TypeParameter node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(UnionType node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(VariableDeclarationExpression node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(VariableDeclarationFragment node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(VariableDeclarationStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(WhileStatement node) {
		error("Unimplemented visit method", node);
		return true;
	}

	@Override
	public boolean visit(WildcardType node) {
		error("Unimplemented visit method", node);
		return true;
	}

	protected static class State {
		public static final int DEFAULT = 0;
		public static final int ASSINGNMENT_LEFT = 1;
		public static final int ASSINGNMENT_RIGHT = 2;
		public static final int RETURN = 4;

		private int state = DEFAULT;

		public State() {
			// create default parsing state
		}

		public boolean isInputDependencyState() {
			return isRight() || isReturn();
		}

		private boolean isSet(int bit) {
			switch (bit) {
			case DEFAULT:
				return state == DEFAULT;
			default:
				break;
			}

			return (state & bit) == bit;
		}

		public boolean isLeft() {
			return isSet(ASSINGNMENT_LEFT);
		}

		public boolean isRight() {
			return isSet(ASSINGNMENT_RIGHT);
		}

		public boolean isReturn() {
			return isSet(RETURN);
		}

		public void setAssignmentLeft(boolean set) {
			set(set, ASSINGNMENT_LEFT);
		}

		public void setAssignmentRight(boolean set) {
			set(set, ASSINGNMENT_RIGHT);
		}

		public void setReturn(boolean set) {
			set(set, RETURN);
		}

		private void set(boolean set, int bit) {
			if (set) {
				setState(state | bit);
			} else {
				if (isSet(bit)) {
					setState(state ^ bit);
				}
			}
		}

		private void setState(int state) {
			this.state = state;
		}

		@Override
		public String toString() {
			if (isDefault()) {
				return "DEFAULT STATE";
			}
			StringBuilder sb = new StringBuilder();
			if (isLeft()) {
				sb.append("ASSIGNMENT_LEFT ");
			}

			if (isRight()) {
				sb.append("ASSIGNMENT_RIGHT ");
			}
			return sb.toString();
		}

		private boolean isDefault() {
			return isSet(DEFAULT);
		}
	}
}
