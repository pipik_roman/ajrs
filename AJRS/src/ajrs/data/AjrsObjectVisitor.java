/**
 * 
 */
package ajrs.data;

import ajrs.data.statement.AjrsBodyStatement;
import ajrs.data.statement.AjrsBreakContinueStatement;
import ajrs.data.statement.AjrsConditionalStatement;
import ajrs.data.statement.AjrsExpressionStatement;
import ajrs.data.statement.AjrsLoopStatement;
import ajrs.data.statement.AjrsMethodBlock;
import ajrs.data.statement.AjrsReturnStatement;
import ajrs.data.statement.AjrsThrowStatement;
import ajrs.data.statement.AjrsTryStatement;
import ajrs.data.statement.AjrsVariableStatement;

/**
 * Visitor pre AJRS Object
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
@SuppressWarnings("unused")
public abstract class AjrsObjectVisitor {

	public boolean visit(AjrsBodyStatement statement) {
		return true;
	}

	public boolean visit(AjrsConditionalStatement statement) {
		return true;
	}

	public void visit(AjrsExpressionStatement statement) {
		// no children
	}

	public boolean visit(AjrsLoopStatement statement) {
		return true;
	}

	public boolean visit(AjrsMethodBlock statement) {
		return true;
	}

	public void visit(AjrsReturnStatement statement) {
		// no children
	}

	public void visit(AjrsThrowStatement statement) {
		// no children
	}

	public boolean visit(AjrsTryStatement statement) {
		return true;
	}

	public void visit(AjrsBreakContinueStatement statement) {
		// no children
	}

	public void visit(AjrsVariableStatement statement) {
		// no children
	}

	public boolean visit(AjrsUnit unit) {
		return true;
	}

	public boolean visit(AjrsType type) {
		return true;
	}

	public boolean visit(AjrsMethod method) {
		return true;
	}

	public void visit(AjrsField field) {
		// no children
	}

	public void afterVisit(AjrsBodyStatement statement) {
		//
	}

	public void afterVisit(AjrsConditionalStatement statement) {
		//
	}

	public void afterVisit(AjrsLoopStatement statement) {
		//
	}

	public void afterVisit(AjrsMethodBlock statement) {
		//
	}

	public void afterVisit(AjrsTryStatement statement) {
		//
	}

	public void afterVisit(AjrsUnit unit) {
		//
	}

	public void afterVisit(AjrsType type) {
		//
	}

	public void afterVisit(AjrsMethod method) {
		//
	}
}
