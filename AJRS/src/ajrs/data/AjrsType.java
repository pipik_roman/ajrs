/**
 * 
 */
package ajrs.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import ajrs.data.statement.AjrsMethodBlock;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
@SuppressWarnings("unchecked")
public class AjrsType extends AjrsObject<AbstractTypeDeclaration> {

	private final List<AjrsField> fields = new ArrayList<AjrsField>();
	private final Map<AjrsHash, AjrsMethod> methods = new HashMap<AjrsHash, AjrsMethod>();

	{
		setSubObjects(fields, methods.values());
	}

	public AjrsType(AjrsUnit unit, AbstractTypeDeclaration type) {
		super(1, 1, unit, type);
		String name = type.getName().getFullyQualifiedName();
		setDescription(name);
		int[] typ = new int[] { name.hashCode() };
		setHash(PART_TYPE, typ);
	}

	/**
	 * Spracujeme polia a metody typu. Najprv si ulozime zoznam poli a metod. Potom prechadzame polia a metody
	 */
	@Override
	public boolean visit(TypeDeclaration node) {
		for (FieldDeclaration f : node.getFields()) {
			AjrsField a = new AjrsField(this, f);
			this.fields.add(a);
			f.accept(a);
		}

		// najprv pridame vsetky metody az tak sa na ne budeme odkazovat
		for (MethodDeclaration m : node.getMethods()) {
			AjrsMethod a = new AjrsMethod(this, m);
			this.methods.put(a.hash, a);
		}

		for (AjrsField a : fields) {
			a.visitPeer();
		}

		for (AjrsMethod a : methods.values()) {
			a.visitPeer();
		}

		for (AjrsMethod a : methods.values()) {
			AjrsMethodBlock block = a.getBlock();
			if (block == null) {
				continue;
			} else {
				block.visitPeer();
			}
		}
		return false;
	}

	public boolean isSameType(int hash) {
		return getHash(0)[0] == hash;
	}

	/**
	 * @return the methods methods map
	 */
	public Map<AjrsHash, AjrsMethod> getMethods() {
		return methods;
	}

	/**
	 * Vrati pocet zavislosti pre dany super typ a daný typ zavislosti methody
	 * 
	 * @param superType
	 *            super typ tejto triedy (trieda alebo interface)
	 * @param methodDependencyPart
	 *            typ zavislosti pre metodu
	 * @return mapa kde kluc je hash fieldu a value pocet zavislosti pre tento field
	 */
	public Map<Integer, Integer> getDependencyCounts(ITypeBinding superType, int methodDependencyPart) {
		assert methodDependencyPart == AjrsMethod.PART_INPUT_DEPENDENCY || methodDependencyPart == AjrsMethod.PART_OUTPUT_DEPENDENCY;
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();

		for (IMethodBinding methodBinding : superType.getDeclaredMethods()) {
			if (methodBinding.isDefaultConstructor()) {
				return map;
			}

			AjrsHash hash = AjrsMethod.createHash(methodBinding);
			AjrsMethod method = methods.get(hash);

			if (method == null) {
				System.err.println("No AjrsMethod found for hash: " + hash + ", searched method: " + methodBinding.getName() + ", args:" + Arrays.toString(methodBinding.getParameterTypes())
						+ ", map contains: " + methods);
				break;
			}

			for (int i : method.hash.getPart(methodDependencyPart)) {
				Integer fieldHash = i;
				if (map.containsKey(fieldHash)) {
					map.put(fieldHash, map.get(fieldHash) + 1);
				} else {
					map.put(fieldHash, 1);
				}
			}
		}

		return map;
	}

	public static AjrsHash createHash(ITypeBinding declaringClass) {
		int[] typ = new int[] { declaringClass.getName().toString().hashCode() };
		return new AjrsHash(1, new int[][] { typ });
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		boolean children = visitor.visit(this);
		if (children) {
			if (fields != null) {
				for (AjrsField field : fields) {
					field.accept(visitor);
				}
			}
			if (methods != null) {
				for (AjrsMethod method : methods.values()) {
					method.accept(visitor);
				}
			}
		}
		visitor.afterVisit(this);
	}

	@Override
	public String getDescription() {
		return super.getDescription();
		// + ",\n Complexities: " + complexities + ",\n Long methods: " + longMethods + ",\n Too many arguments: "
		// + tooManyArguments + ", \n";
	}

	public List<AjrsField> getFields() {
		return fields;
	}

	@Override
	public AjrsType getAjrsType() {
		return this;
	}
}
