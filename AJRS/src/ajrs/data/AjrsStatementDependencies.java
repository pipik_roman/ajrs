/**
 * 
 */
package ajrs.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ajrs.data.statement.AjrsStatement;

/**
 * Obsahuje aktualne zavislosti.
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsStatementDependencies {
	List<HashMap<Integer, AjrsStatement>> stack = new ArrayList<HashMap<Integer, AjrsStatement>>();
	private HashMap<Integer, AjrsStatement> dependencyMap = new HashMap<Integer, AjrsStatement>();

	public AjrsStatementDependencies() {

	}

	// TODO goInto a goOut by malo brat parameter do coho ide, podla toho by sme vedeli lahsie najst parent dependency!!!!
	public void goInto() {
		// HashMap<Integer, AjrsStatement> dependencyMap = new HashMap<Integer, AjrsStatement>(this.dependencyMap);
		// stack.add(this.dependencyMap);
		// this.dependencyMap = dependencyMap;
		// dependencies remains active in parent statements
	}

	public void goOut() {
		// this.dependencyMap = stack.remove(stack.size() - 1);
		// dependencies remains active in parent statements
	}

	/**
	 * Prida vsetky vystupne zavislosti tohto statementu. Stare dependency sa prepisu novymi!
	 * 
	 * @param statement
	 *            statement ktoreho dependency sa maju pridat
	 */
	public void addOutputVariableDependencies(AjrsStatement statement) {
		int[] outputVariableDependencies = statement.getHash(AjrsStatement.PART_OUTPUT_VARIABLE_DEPENDENCY);
		if (outputVariableDependencies != null) {
			for (int outputVariableDependency : outputVariableDependencies) {
				dependencyMap.put(outputVariableDependency, statement);
			}
		}
	}

	/**
	 * Vrati statement ktory ma zavislost na variable. Nekontroluje sa ci je mimo parenta!
	 * 
	 * @param inputVariableDependency
	 *            hash pre variable (pochadza z input variable dependenncy nejakeho statementu)
	 * @return statement zavislosti, alebo null ak nema zavislost
	 */
	public AjrsStatement getDependendency(int inputVariableDependency) {
		return this.dependencyMap.get(inputVariableDependency);
	}

	/**
	 * Vrati celkovu mapu zavislosti
	 * 
	 * @return the dependencyMap mapa zavislosti
	 */
	public HashMap<Integer, AjrsStatement> getDependencyMap() {
		return dependencyMap;
	}

	/**
	 * Vrati zoznam vsetkych zavislosti ktore naslo.
	 * 
	 * @param statement
	 *            statement ktory ma inputVariable zavislosti
	 * @param inputVariableDependencies
	 *            vstupne zavislosti ktore sa maju hladat
	 * @return zoznam zavislosti ktore naslo, alebo null
	 */
	public List<Integer> getStatementDependencyHashes(AjrsStatement statement, int[] inputVariableDependencies) {
		List<Integer> set = new ArrayList<Integer>();
		for (int inputVariableDependency : inputVariableDependencies) {
			AjrsStatement dependencyStatement = getDependendency(inputVariableDependency);
			if (dependencyStatement != null) {
				// dependencyStatement must be peer with statement. If it is not. We have to find peer
				dependencyStatement = (AjrsStatement) statement.getPeerWith(dependencyStatement);
				set.add(dependencyStatement.hashCode());
			}
		}
		return set;
	}

	/**
	 * Zmaze zavislosti. Zavislosti je treba zmazat ked prechadzame na novu metodu!
	 */
	public void clear() {
		this.stack.clear();
		this.dependencyMap.clear();
	}
}
