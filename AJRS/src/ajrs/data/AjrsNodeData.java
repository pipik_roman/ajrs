/**
 * 
 */
package ajrs.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Parametre, vstupne a vystupne zavislosti uzla
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsNodeData {
	private final List<Integer> parameters = new ArrayList<Integer>();
	private final Set<Integer> inputDependencies = new HashSet<Integer>();
	private final Set<Integer> outputDependencies = new HashSet<Integer>();

	public void addInputDependency(Integer fieldHash) {
		this.inputDependencies.add(fieldHash);
	}

	public void addOutputDependency(Integer fieldHash) {
		assert fieldHash != null;
		this.outputDependencies.add(fieldHash);
	}

	/**
	 * @return the inputDependencies
	 */
	public Set<Integer> getInputDependencies() {
		return inputDependencies;
	}

	/**
	 * @return the outputDependencies
	 */
	public Set<Integer> getOutputDependencies() {
		return outputDependencies;
	}

	/**
	 * @return the parameters
	 */
	public List<Integer> getParameters() {
		return parameters;
	}

	public void addOutputDependency(List<Integer> variables) {
		for (Integer i : variables) {
			addOutputDependency(i);
		}
	}

	public void addInputDependency(List<Integer> variables) {
		for (Integer i : variables) {
			addInputDependency(i);
		}
	}

	public void addInputDependency(int[] variables) {
		for (Integer i : variables) {
			addInputDependency(i);
		}
	}

	public void addOutputDependency(int[] variables) {
		for (Integer i : variables) {
			addOutputDependency(i);
		}
	}

	public void addParameter(Integer parameter) {
		this.parameters.add(parameter);
	}
}
