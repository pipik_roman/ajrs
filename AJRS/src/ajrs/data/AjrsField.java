/**
 * 
 */
package ajrs.data;

import java.util.List;

import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsField extends AjrsObject<FieldDeclaration> {
	private String name;

	public AjrsField(AjrsType type, FieldDeclaration f) {
		super(1, 1, type, f);
	}

	@Override
	public boolean visit(FieldDeclaration node) {
		StringBuilder sb = new StringBuilder();

		@SuppressWarnings("unchecked")
		List<VariableDeclarationFragment> list = node.fragments();
		Type type = node.getType();
		assert list.size() == 1 : "Doesn't suppport multiple field declarations as one";
		for (VariableDeclarationFragment f : list) {
			name = f.getName().getFullyQualifiedName();
		}

		int[] typ = new int[] { name.hashCode(), type.toString().hashCode() };

		sb.append(type.toString() + " " + name);
		setHash(0, typ);
		setDescription(sb.toString());
		return false;
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		visitor.visit(this);
	}

	public String getName() {
		return name;
	}
}
