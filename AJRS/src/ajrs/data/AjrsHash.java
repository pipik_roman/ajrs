/**
 * 
 */
package ajrs.data;

import java.util.Arrays;
import java.util.Iterator;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsHash implements Iterable<int[]> {

	private final int[][] hash;
	private final int uniquePart;
	private int total = 0;

	public AjrsHash(int uniquePart, int[][] parts) {
		assert uniquePart <= parts.length;
		this.uniquePart = uniquePart;
		this.hash = parts;
		updateTotal();
	}

	/**
	 * Vytvori prazdny hash
	 * 
	 * @param uniquePart
	 *            pocet casti ktore su vyznamne (robi sa podla nich equals)
	 * @param hashParts
	 *            pocet blokov hash
	 */
	public AjrsHash(int uniquePart, int hashParts) {
		assert uniquePart <= hashParts;
		this.uniquePart = uniquePart;
		this.hash = new int[hashParts][];
	}

	protected void updateTotal() {
		int total = 0;
		for (int i = 0; i < this.uniquePart; i++) {
			int[] part = hash[i];
			if (part == null) {
				continue;
			}
			for (int value : part) {
				total = total + value;
			}
		}
		this.total = total;
	}

	public void setPart(int index, int[] hash) {
		this.hash[index] = hash;
		updateTotal();
	}

	public int[] getPart(int part) {
		return hash[part];
	}

	@Override
	public int hashCode() {
		return (int) total;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AjrsHash)) {
			return false;
		}
		AjrsHash other = (AjrsHash) obj;
		if (this.uniquePart != other.uniquePart) {
			return false;
		}
		if (this.total != other.total) {
			return false;
		}
		for (int i = 0; i < this.uniquePart; i++) {
			if (!Arrays.equals(hash[i], other.hash[i])) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Iterator<int[]> iterator() {
		return new Iterator<int[]>() {
			int index = 0;

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

			@Override
			public int[] next() {
				return hash[index++];
			}

			@Override
			public boolean hasNext() {
				return index < hash.length;
			}
		};
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(Integer.toHexString(total) + "=");
		for (int[] part : this) {
			builder.append("| ");
			for (int i : part) {
				builder.append(Integer.toHexString(i));
				builder.append(", ");
			}
		}
		return builder.toString().toUpperCase();
	}
}
