/**
 * 
 */
package ajrs.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.SimpleName;

import ajrs.builder.AjrsBuilder;
import ajrs.data.smells.AjrsDepthComplexityFinder;
import ajrs.data.smells.AjrsLargeClassFinder;
import ajrs.data.smells.AjrsLazyClassFinder;
import ajrs.data.smells.AjrsLongMethodFinder;
import ajrs.data.smells.AjrsMultiplePersonalityFinder;
import ajrs.data.smells.AjrsSizeComplexityFinder;
import ajrs.data.smells.AjrsSmellCandidate;
import ajrs.data.smells.AjrsSmellFinder;
import ajrs.data.smells.AjrsSpecialCaseFinder;
import ajrs.data.smells.AjrsTooManyArgumentsFinder;
import ajrs.data.smells.duplicatedCode.AjrsDuplicatedCodeFinder;
import ajrs.data.smells.duplicatedCode.AjrsDuplicationAnalysis;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
@SuppressWarnings("unchecked")
public class AjrsUnit extends AjrsObject<CompilationUnit> {
	private long parseStart = System.nanoTime();
	private static final List<AjrsSmellFinder<? extends AjrsSmellCandidate>> finders = new ArrayList<AjrsSmellFinder<? extends AjrsSmellCandidate>>();
	private static final String MARKER_TYPE = "AJRS.ajrsMarker";

	static {
		finders.add(new AjrsDepthComplexityFinder());
		finders.add(new AjrsLargeClassFinder());
		finders.add(new AjrsLazyClassFinder());
		finders.add(new AjrsLongMethodFinder());
		finders.add(new AjrsMultiplePersonalityFinder());
		finders.add(new AjrsSizeComplexityFinder());
		finders.add(new AjrsSpecialCaseFinder());
		finders.add(new AjrsTooManyArgumentsFinder());
		finders.add(new AjrsDuplicatedCodeFinder());
	}

	private final List<AjrsType> types = new ArrayList<AjrsType>();
	private final Map<AjrsType, AjrsHash> personalities = new HashMap<AjrsType, AjrsHash>();
	private final List<AjrsSmellCandidate> smells = new ArrayList<AjrsSmellCandidate>();
	private final AjrsDuplicationAnalysis duplicationAnalysis = new AjrsDuplicationAnalysis();
	private final IFile file;

	{
		setSubObjects(types);
	}

	public AjrsUnit(CompilationUnit unit, IFile file) {
		super(0, 0, null, unit);
		this.file = file;
		deleteMarkersAndMetadata();
	}

	public AjrsDuplicationAnalysis getDuplicationAnalysis() {
		return duplicationAnalysis;
	}

	/**
	 * @return
	 */
	private String getResultMessage() {
		long time = (System.nanoTime() - parseStart) / 1000000;
		return "Processed by AJRS (" + time + "ms)";
	}

	@Override
	public boolean visit(CompilationUnit node) {
		setDescription(node.getTypeRoot().getElementName());
		List<AbstractTypeDeclaration> types = node.types();
		for (AbstractTypeDeclaration type : types) {
			this.types.add(new AjrsType(this, type));
		}
		for (AjrsType type : this.types) {
			type.visitPeer();
		}

		this.duplicationAnalysis.analyse(this);
		// duplication analysis je dostupna pre pachy
		findSmells();
		addSmellMarkers();
		addMarker(getResultMessage(), 1, IMarker.SEVERITY_INFO, false);
		return false;
	}

	private void findSmells() {
		for (AjrsSmellFinder<? extends AjrsSmellCandidate> finder : finders) {
			findSmells(finder);
		}
	}

	public List<AjrsSmellCandidate> getSmells() {
		return smells;
	}

	private void findSmells(AjrsSmellFinder<? extends AjrsSmellCandidate> finder) {
		List<? extends AjrsSmellCandidate> smells = finder.find(this);
		this.smells.addAll(smells);
	}

	public Map<AjrsType, AjrsHash> getDoublePersonalities() {
		return personalities;
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		boolean children = visitor.visit(this);
		if (children) {
			if (types != null) {
				for (AjrsType type : types) {
					type.accept(visitor);
				}
			}
		}
		visitor.afterVisit(this);
	}

	@Override
	public String getDescription() {
		StringBuilder builder = new StringBuilder(super.getDescription());
		if (!personalities.isEmpty()) {
			builder.append("\n Multiple Personalities: ");
			for (AjrsHash h : personalities.values()) {
				builder.append("\n" + h);
			}
		}
		return builder.toString();
	}

	public List<AjrsType> getTypes() {
		return types;
	}

	@Override
	public AjrsUnit getAjrsUnit() {
		return this;
	}

	@Override
	public IFile getFile() {
		return file;
	}

	@Override
	public IMarker addMarker(String message, int start, int length, int severity, boolean isTransient) {
		try {
			IMarker marker = file.createMarker(MARKER_TYPE);
			marker.setAttribute(IMarker.MESSAGE, message);
			marker.setAttribute(IMarker.SEVERITY, severity);
			marker.setAttribute(IMarker.CHAR_START, start);
			marker.setAttribute(IMarker.CHAR_END, start + length);
			marker.setAttribute(IMarker.TRANSIENT, true);
			return marker;
		} catch (CoreException e) {
			e.printStackTrace();
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public void addMarker(String message, int lineNumber, int severity, boolean isTransient) {
		try {
			IMarker marker = file.createMarker(MARKER_TYPE);
			marker.setAttribute(IMarker.MESSAGE, message);
			marker.setAttribute(IMarker.SEVERITY, severity);
			marker.setAttribute(IMarker.TRANSIENT, true);
			if (lineNumber == -1) {
				lineNumber = 1;
			}
			marker.setAttribute(IMarker.LINE_NUMBER, lineNumber);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addLineMarker(String message, int position, int severity, boolean isTransient) {
		CompilationUnit cu = getPeer();
		int lineNumber = cu.getLineNumber(position);
		addMarker(message, lineNumber, severity, isTransient);
	}

	private void deleteMarkersAndMetadata() {
		try {
			file.deleteMarkers(MARKER_TYPE, false, IResource.DEPTH_ZERO);
		} catch (CoreException ce) {
			ce.printStackTrace();
		}
	}

	public void addSmellMarkers() {
		for (AjrsSmellCandidate candidate : getSmells()) {
			AjrsObject<? extends ASTNode> ajrsNode = candidate.getNode();
			final int start;
			final int length;
			if (ajrsNode instanceof AjrsType) {
				AjrsType type = (AjrsType) candidate.getNode();
				SimpleName name = type.getPeer().getName();
				start = name.getStartPosition();
				length = name.getLength();
			} else if (ajrsNode instanceof AjrsMethod) {
				AjrsMethod method = (AjrsMethod) candidate.getNode();
				SimpleName name = method.getPeer().getName();
				start = name.getStartPosition();
				length = name.getLength();
			} else {
				ASTNode node = ajrsNode.getPeer();
				start = node.getStartPosition();
				length = node.getLength();

			}
			IMarker marker = addMarker(candidate.toString(), start, length, IMarker.SEVERITY_WARNING, false);
			AjrsBuilder.markerToSmell.put(marker, candidate);
		}
	}
}
