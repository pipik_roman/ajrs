/**
 * 
 */
package ajrs.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;

import ajrs.EmptyIterator;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <AstPeer>
 *            peer object in ast
 */
public abstract class AjrsObject<AstPeer extends ASTNode> extends ASTVisitor implements Iterable<AjrsObject<? extends ASTNode>> {
	protected List<Collection<? extends AjrsObject<? extends ASTNode>>> subObjects;

	public static final int PART_TYPE = 0;

	private final AjrsObject<? extends ASTNode> parent;
	private final AstPeer peer;
	AjrsHash hash;
	private boolean visited = false;
	private String description;

	private final Map<Integer, Object> properties = new HashMap<Integer, Object>();

	/**
	 * Create object from type, type is processed, not stored
	 * 
	 * @param uniqueParts
	 *            pocet casti hash ktore tvoria jeho identitu
	 * @param hashParts
	 *            number of hash parts
	 * @param parent
	 *            parent of this object, or null
	 * @param peer
	 *            peer object in ast
	 */
	public AjrsObject(int uniqueParts, int hashParts, AjrsObject<?> parent, AstPeer peer) {
		if (peer == null) {
			throw new IllegalArgumentException("Peer cannot be null for " + this);
		}
		this.parent = parent;
		this.peer = peer;
		this.hash = new AjrsHash(uniqueParts, hashParts);
	}

	public AjrsObject<? extends ASTNode> getParent() {
		return parent;
	}

	public void setSubObjects(Collection<? extends AjrsObject<? extends ASTNode>>... subObjects) {
		assert subObjects != null;
		this.subObjects = new ArrayList<Collection<? extends AjrsObject<? extends ASTNode>>>();
		for (Collection<? extends AjrsObject<?>> subObject : subObjects) {
			this.subObjects.add(subObject);
		}
	}

	public String getDescription() {
		return description;
	}

	public AjrsHash getHash() {
		return hash;
	}

	public AstPeer getPeer() {
		return peer;
	}

	public int[] getHash(int part) {
		return this.hash.getPart(part);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setHash(int part, int[] hash) {
		this.hash.setPart(part, hash);
	}

	public void print() {
		System.out.print(getDescription() + " : ");
		System.out.print(hash.toString());

		Iterator<AjrsObject<?>> iterator = this.iterator();
		while (iterator.hasNext()) {
			System.out.println();
			iterator.next().print();
		}
	}

	@Override
	public Iterator<AjrsObject<?>> iterator() {
		if (subObjects == null) {
			return new EmptyIterator<AjrsObject<?>>();
		}

		return new Iterator<AjrsObject<? extends ASTNode>>() {

			private final Iterator<Collection<? extends AjrsObject<? extends ASTNode>>> iterator = subObjects.iterator();
			private Iterator<? extends AjrsObject<? extends ASTNode>> subIterator = null;

			@Override
			public boolean hasNext() {
				while (subIterator == null || !subIterator.hasNext()) {
					if (iterator.hasNext()) {
						subIterator = iterator.next().iterator();
					} else {
						break;
					}
				}
				return subIterator != null && subIterator.hasNext();
			}

			@Override
			public AjrsObject<? extends ASTNode> next() {
				return subIterator.next();
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	/**
	 * Visit AST peer, with protection from recursive calls
	 */
	public void visitPeer() {
		if (visited) {
			return;
		}
		this.visited = true;
		peer.accept(this);
	}

	public final void error(String message, Object... objects) {
		StringBuilder b = new StringBuilder(message);
		for (Object object : objects) {
			b.append(", " + object.getClass() + "=" + object);
		}
		throw new AssertionError(b.toString());
	}

	public abstract void accept(AjrsObjectVisitor visitor);

	@Override
	public String toString() {
		return getDescription();
	}

	@Override
	public int hashCode() {
		return hash.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return hash.equals(obj);
	}

	public void setProperty(Integer nameHash, Object object) {
		this.properties.put(nameHash, object);
	}

	public Object getProperty(Integer nameHash) {
		return this.properties.get(nameHash);
	}

	public String printPeer() {
		return peer.toString();
	}

	/**
	 * Returns {@link AjrsUnit} of actual tree. This method goes back to parents until {@link AjrsUnit} is reached and returned
	 * 
	 * @return {@link AjrsUnit} of actual tree.
	 */
	public AjrsUnit getAjrsUnit() {
		return parent.getAjrsUnit();
	}

	/**
	 * Returns {@link AjrsType} of actual tree. This method goes back to parents until {@link AjrsType} is reached and returned
	 * 
	 * @return {@link AjrsType} of actual tree.
	 */
	public AjrsType getAjrsType() {
		return parent.getAjrsType();
	}

	/**
	 * Returns {@link AjrsMethod} of actual tree. This method goes back to parents until {@link AjrsMethod} is reached and returned
	 * 
	 * @return {@link AjrsMethod} of actual tree.
	 */
	public AjrsMethod getAjrsMethod() {
		return parent.getAjrsMethod();
	}

	/**
	 * Returns file for this AST object.
	 * 
	 * @return file for this AST object
	 */
	public IFile getFile() {
		return parent.getAjrsUnit().getFile();
	}

	/**
	 * Returns peer of two statements. Two statements are peers when they have same parent. Traverses through other object parents while one of parents is not peer with this object
	 * 
	 * @param other
	 *            object to find peer
	 * @return
	 */
	public AjrsObject<? extends ASTNode> getPeerWith(AjrsObject<? extends ASTNode> other) {
		ASTNode otherParent = other.getPeer().getParent();
		ASTNode thisParent = getPeer().getParent();
		if (otherParent == null || thisParent == null) {
			throw new AssertionError("getPeerWith failed, thisParent thisParent==" + thisParent + ", otherParent==" + otherParent);
		}
		if (thisParent == otherParent) {
			return other; // rodic je rovnaky - su peer
		} else {
			System.err.println(other.getPeer() + " nieje peer" + this.getPeer());
			return getPeerWith(other.getParent());
		}
	}

	/**
	 * Add marker from start position to start + length position
	 * 
	 * @param message
	 *            message of marker
	 * @param start
	 *            start position
	 * @param length
	 *            length of marker
	 * @param severity
	 *            severity of marker
	 */
	public IMarker addMarker(String message, int start, int length, int severity, boolean isTransient) {
		return parent.getAjrsUnit().addMarker(message, start, length, severity, isTransient);
	}

	/**
	 * Add line marker for specified line position
	 * 
	 * @param message
	 *            message of marker
	 * @param lineNumber
	 * @param severity
	 *            severity of marker
	 */
	public void addMarker(String message, int lineNumber, int severity, boolean isTransient) {
		parent.getAjrsUnit().addMarker(message, lineNumber, severity, isTransient);
	}

	/**
	 * Add line marker for specifified position in code
	 * 
	 * @param message
	 *            message of marker
	 * @param position
	 *            position to translate into line
	 * @param severity
	 *            severity of marker
	 */
	public void addLineMarker(String message, int position, int severity, boolean isTransient) {
		parent.getAjrsUnit().addLineMarker(message, position, severity, isTransient);
	}
}
