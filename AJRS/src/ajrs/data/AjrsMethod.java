/**
 * 
 */
package ajrs.data;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AssertStatement;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.BreakStatement;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.ContinueStatement;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EmptyStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.ThrowStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;

import ajrs.AjrsUtils;
import ajrs.data.statement.AjrsMethodBlock;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
@SuppressWarnings("unchecked")
public class AjrsMethod extends AjrsNode<MethodDeclaration> {

	public static final int PART_INPUT_DEPENDENCY = 2;
	public static final int PART_OUTPUT_DEPENDENCY = 3;

	private final List<AjrsMethodBlock> blocks = new ArrayList<AjrsMethodBlock>();
	protected final AjrsStatementDependencies statementDependency = new AjrsStatementDependencies();

	{
		setSubObjects(blocks);
	}

	public AjrsMethod(AjrsType parent, MethodDeclaration peer) {
		super(2, 4, parent, peer, false);

		AjrsHash hash = createHash(peer.resolveBinding());
		setHash(PART_TYPE, hash.getPart(PART_TYPE));
		setHash(PART_PARAMETERS, hash.getPart(PART_PARAMETERS));

		StringBuilder sb = new StringBuilder();
		String name = peer.getName().getFullyQualifiedName();
		sb.append(name + "(");

		List<SingleVariableDeclaration> list = peer.parameters();
		for (int i = 0; i < list.size(); i++) {
			Type type = list.get(i).getType();
			if (i != 0) {
				sb.append(",");
			}
			sb.append(type);
		}
		sb.append(")");

		setDescription(sb.toString());
	}

	public AjrsMethodBlock getBlock() {
		assert this.blocks.size() <= 1;
		if (this.blocks.isEmpty()) {
			return null;
			// TODO adept na NullObject
		} else {
			return this.blocks.get(0);
		}
	}

	@Override
	protected void nodeDataToHash() {
		AjrsNodeData nodeData = getNodeData();
		int[] inDep = AjrsUtils.asIntArray(nodeData.getInputDependencies());
		int[] outDep = AjrsUtils.asIntArray(nodeData.getOutputDependencies());
		setHash(PART_INPUT_DEPENDENCY, inDep);
		setHash(PART_OUTPUT_DEPENDENCY, outDep);
	}

	@Override
	public void processField(IVariableBinding variable, boolean isDeclaration) {
		if (!isDeclaration) {
			super.processField(variable, isDeclaration);
		}
	}

	@Override
	protected void processLocal(IVariableBinding variable, boolean isDeclaration) {
		// not interested in local, only fields
	}

	@Override
	protected void processParameter(IVariableBinding variable, boolean isDeclaration) {
		// not interested in parameters, only fields
	}

	@Override
	protected void processMethodBinding(IMethodBinding method) {
		// not interested in method binding - v buducnosti mozeme dvojbehovo robit zavislosti cez metody
	}

	@Override
	public boolean visit(MethodDeclaration node) {
		processBody(node);
		nodeDataToHash();
		return false;
	}

	private void processBody(MethodDeclaration node) {
		Block block = node.getBody();
		if (block != null) {// block je null ak to je rozhranie
			AjrsMethodBlock ajrsBlock = new AjrsMethodBlock(this, block);
			blocks.add(ajrsBlock);
			block.accept(this);
		}
	}

	@Override
	public boolean visit(ExpressionStatement node) {
		Expression expression = node.getExpression();
		expression.accept(this);
		return false;
	}

	@Override
	public boolean visit(ReturnStatement node) {
		assert !state.isReturn();
		state.setReturn(true);
		Expression expression = node.getExpression();
		if (expression != null) {
			expression.accept(this);
		}
		state.setReturn(false);
		return false;
	}

	@Override
	public boolean visit(Block node) {
		List<Statement> statements = node.statements();
		for (Statement statement : statements) {
			statement.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(IfStatement node) {
		Expression expression = node.getExpression();
		expression.accept(this);
		Statement thenStatement = node.getThenStatement();
		thenStatement.accept(this);
		Statement elseStatement = node.getElseStatement();
		if (elseStatement != null) {
			elseStatement.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(VariableDeclarationStatement node) {
		List<VariableDeclarationFragment> fragments = node.fragments();
		for (VariableDeclarationFragment fragment : fragments) {
			fragment.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(ForStatement node) {
		List<Expression> initializers = node.initializers();
		for (Expression initializer : initializers) {
			initializer.accept(this);
		}

		Expression expression = node.getExpression();
		expression.accept(this);

		List<Expression> updaters = node.updaters();
		for (Expression updater : updaters) {
			updater.accept(this);
		}

		Statement body = node.getBody();
		body.accept(this);
		return false;
	}

	@Override
	public boolean visit(DoStatement node) {
		node.getBody().accept(this);
		Expression expression = node.getExpression();
		expression.accept(this);
		return false;
	}

	@Override
	public boolean visit(WhileStatement node) {
		node.getExpression().accept(this);
		node.getBody().accept(this);
		return false;
	}

	@Override
	public boolean visit(EnhancedForStatement node) {
		node.getExpression().accept(this);
		node.getBody().accept(this);
		return false;
	}

	@Override
	public boolean visit(TryStatement node) {
		node.getBody().accept(this);
		List<CatchClause> catchClauses = node.catchClauses();
		for (CatchClause catchClause : catchClauses) {
			// ignorujeme deklaraciu exception
			catchClause.getBody().accept(this);
		}
		Block finallyBlock = node.getFinally();
		if (finallyBlock != null) {
			finallyBlock.accept(this);
		}

		return false;
	}

	@Override
	public boolean visit(ThrowStatement node) {
		node.getExpression().accept(this);
		return false;
	}

	@Override
	public boolean visit(org.eclipse.jdt.core.dom.SwitchStatement node) {
		Expression expression = node.getExpression();
		expression.accept(this);

		List<Statement> statements = node.statements();
		for (Statement statement : statements) {
			statement.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(SwitchCase node) {
		Expression expression = node.getExpression();
		if (expression != null) {
			expression.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(ContinueStatement node) {
		// ignore
		return false;
	}

	/**
	 * Vytvorí rovnaký hash ako sa používa pri {@link AjrsMethod}, s typom a parametrami
	 * 
	 * @param methodBinding
	 *            method binding pre ktorý chceme
	 * @return hash z typu a parametrov
	 */
	public static AjrsHash createHash(IMethodBinding methodBinding) {
		if (methodBinding == null) {
			return new AjrsHash(2, new int[][] { new int[] {}, new int[] {} });
		}
		ITypeBinding[] parameterTypes = methodBinding.getParameterTypes();
		int[] typ = new int[] { methodBinding.getName().hashCode() };
		int[] input = new int[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			input[i] = parameterTypes[i].getQualifiedName().hashCode();
		}
		return new AjrsHash(2, new int[][] { typ, input });
	}

	// ------------------------------------IGNOROVANE STATEMENTS-----------------------------------------//
	@Override
	public boolean visit(EmptyStatement node) {
		// ignore
		return false;
	}

	@Override
	public boolean visit(BreakStatement node) {
		// ignore
		return false;
	}

	@Override
	public boolean visit(SuperConstructorInvocation node) {
		// ignore - super konstruktor nas nezaujima lebo moze pouzivat iba static veci a musi byt vzdy prvy
		return false;
	}

	@Override
	public boolean visit(AssertStatement node) {
		// ignore
		return false;
	}

	// ------------------------------------IGNOROVANE EXPRESSIONS-----------------------------------------//

	@Override
	public AjrsType getType() {
		return (AjrsType) getParent();
	}

	public int[] getInputDependency() {
		return getHash(PART_INPUT_DEPENDENCY);
	}

	public int[] getOutputDependency() {
		return getHash(PART_OUTPUT_DEPENDENCY);
	}

	@Override
	public void accept(AjrsObjectVisitor visitor) {
		boolean children = visitor.visit(this);
		if (children) {
			if (blocks != null) {
				assert blocks.size() <= 1;// there should be one block or no block
				if (blocks.isEmpty()) {
					return;
				} else {
					blocks.get(0).accept(visitor);
				}
			}
		}
		visitor.afterVisit(this);
	}

	@Override
	public AjrsMethod getAjrsMethod() {
		return this;
	}
}
