/**
 * 
 */
package ajrs.data.smells;

import java.util.ArrayList;
import java.util.List;

import ajrs.Activator;
import ajrs.data.AjrsMethod;
import ajrs.data.AjrsNode;
import ajrs.data.AjrsUnit;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsTooManyArgumentsFinder extends AjrsSmellFinder<AjrsTooManyArguments> {

	private final List<AjrsTooManyArguments> smells = new ArrayList<AjrsTooManyArguments>();

	public AjrsTooManyArgumentsFinder() {
		//
	}

	@Override
	public List<AjrsTooManyArguments> find(AjrsUnit unit) {
		this.smells.clear();
		unit.accept(this);
		return smells;
	}

	@Override
	public boolean visit(AjrsMethod method) {
		int arguments = method.getHash(AjrsNode.PART_PARAMETERS).length;
		if (arguments > getScoreToIdentify()) {
			AjrsTooManyArguments smell = new AjrsTooManyArguments(method);
			smells.add(smell);
		}
		return false;
	}

	private int getScoreToIdentify() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.TOO_MANY_ARGUMENTS_LIMIT);
	}

}
