/**
 * 
 */
package ajrs.data.smells;

import java.util.Set;

import org.eclipse.jdt.core.dom.ITypeBinding;

import ajrs.AjrsUtils;
import ajrs.data.AjrsHash;
import ajrs.data.AjrsType;

/**
 * Pach viacerých osobností - podľa rozhraní
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsMultiplePersonality extends AjrsSmellCandidate {

	public static final int PART_PRIMARY_PERSONALITY = 0;
	public static final int PART_SECONDARY_PERSONALITIES = 1;
	public static final int PART_IGNORED_PERSONALITIES = 2;

	public AjrsMultiplePersonality(AjrsType node, Integer primaryPersonality, Set<Integer> secondaryPersonalities, Set<Integer> ignoredPersonalities) {
		super(2, 3, MULTIPLE_PERSONALITY, node);
		assert secondaryPersonalities != null;
		assert ignoredPersonalities != null;

		if (primaryPersonality == null) {
			setPart(PART_PRIMARY_PERSONALITY, new int[] {});
		}
		else {
			setPart(PART_PRIMARY_PERSONALITY, new int[] { primaryPersonality });
		}

		setPart(PART_SECONDARY_PERSONALITIES, AjrsUtils.asIntArray(secondaryPersonalities));
		setPart(PART_IGNORED_PERSONALITIES, AjrsUtils.asIntArray(ignoredPersonalities));
	}

	public boolean hasPrimary() {
		return getPart(PART_PRIMARY_PERSONALITY).length > 0;
	}

	public boolean hasSecondary() {
		return getPart(PART_SECONDARY_PERSONALITIES).length > 0;
	}

	@Override
	public String toString() {
		return "This type has multiple personalities! Primary personality: " + getPrimaryPersonality() + ". Secondary personalities:"
				+ getSecondaryPersonalities();
	}

	private String getSecondaryPersonalities() {
		StringBuilder sb = new StringBuilder();
		for (int i : getPart(PART_SECONDARY_PERSONALITIES)) {
			sb.append(nameOf(i) + ", ");
		}
		return sb.toString();
	}

	private String getPrimaryPersonality() {
		int[] primary = getPart(PART_PRIMARY_PERSONALITY);
		if (primary == null || primary.length != 1) {
			return "not found";
		}
		else {
			return nameOf(primary[0]);
		}
	}

	private String nameOf(int i) {
		ITypeBinding[] interfaces = getNode().getPeer().resolveBinding().getInterfaces();
		for (ITypeBinding binding : interfaces) {
			AjrsHash hash = AjrsType.createHash(binding);
			if (hash.hashCode() == i) { return binding.getQualifiedName(); }
		}
		throw new AssertionError();
	}

	@Override
	public AjrsType getNode() {
		return (AjrsType) super.getNode();
	}
}
