/**
 * 
 */
package ajrs.data.smells;

import ajrs.data.statement.AjrsStatement;

/**
 * Pach pre hĺbkovú zložitosť - teda koeficient vnorených výrazov
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsDepthComplexity extends AjrsSmellCandidate {
	public AjrsDepthComplexity(AjrsStatement node) {
		super(2, 2, DEPTH_COMPLEXITY, node);
	}

	@Override
	public String toString() {
		return "Code in this method is too complex in depth (nested conditials, loops, etc.) ";
	}

}
