/**
 * 
 */
package ajrs.data.smells;

import org.eclipse.jdt.core.dom.ASTNode;

import ajrs.data.AjrsObject;

/**
 * Pach veľkostnej náročnosti, kontroluje počet statementov v nekoncových uzloch. Líši sa trocha od pachu LongMethod
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsSizeComplexity extends AjrsSmellCandidate {
	public AjrsSizeComplexity(AjrsObject<? extends ASTNode> node) {
		super(2, 2, SIZE_COMPLEXITY, node);
	}

	@Override
	public String toString() {
		return "Code in this method is too complex in size (too many complex statements) ";
	}
}
