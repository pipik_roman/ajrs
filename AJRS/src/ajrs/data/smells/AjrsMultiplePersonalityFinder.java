/**
 * 
 */
package ajrs.data.smells;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.jdt.core.dom.ITypeBinding;

import ajrs.data.AjrsHash;
import ajrs.data.AjrsMethod;
import ajrs.data.AjrsType;
import ajrs.data.AjrsUnit;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsMultiplePersonalityFinder extends AjrsSmellFinder<AjrsMultiplePersonality> {
	public static final double SCORE_TO_IGNORE = 1.0;
	public static final double SCORE_FOR_INPUT = 0.5;
	public static final double SCORE_FOR_OUTPUT = 0.5;
	public static final double PRIMARY_PERSONALITY_DISTANCE = 1.3;// percent/100

	private final List<AjrsMultiplePersonality> smells = new ArrayList<AjrsMultiplePersonality>();

	@Override
	public List<AjrsMultiplePersonality> find(AjrsUnit unit) {
		this.smells.clear();
		unit.accept(this);
		return smells;
	}

	@Override
	public boolean visit(AjrsType type) {
		AjrsMultiplePersonality smell = find(type);
		if (smell.hasPrimary()) {
			if (smell.getPart(AjrsMultiplePersonality.PART_SECONDARY_PERSONALITIES).length >= 1) {
				this.smells.add(smell);
			}
		}
		else if (smell.hasSecondary()) {
			if (smell.getPart(AjrsMultiplePersonality.PART_SECONDARY_PERSONALITIES).length > 1) {
				this.smells.add(smell);
			}
		}
		return false;
	}

	/**
	 * Vyhodnoti multiple personality
	 * 
	 * @param type
	 *            typ pre spracovanie
	 * @return multiple personality status
	 */
	public AjrsMultiplePersonality find(AjrsType type) {
		Set<Integer> secondary = new HashSet<Integer>();
		Set<Integer> ignored = new HashSet<Integer>();
		Integer primary = null;

		ITypeBinding typeBinding = type.getPeer().resolveBinding();
		ITypeBinding[] interfaces = typeBinding.getInterfaces();

		Map<ITypeBinding, Double> scoreMap = new HashMap<ITypeBinding, Double>();

		for (ITypeBinding binding : interfaces) {
			Map<Integer, Integer> input = type.getDependencyCounts(binding, AjrsMethod.PART_INPUT_DEPENDENCY);
			Map<Integer, Integer> output = type.getDependencyCounts(binding, AjrsMethod.PART_OUTPUT_DEPENDENCY);
			double score = countScore(input, output);
			scoreMap.put(binding, score);
		}
		double bestScore = 0.0;
		Integer best = null;
		double bestDistance = 0.0;

		for (Entry<ITypeBinding, Double> entry : scoreMap.entrySet()) {
			double score = entry.getValue();
			AjrsHash hash = AjrsType.createHash(entry.getKey());
			if (score <= SCORE_TO_IGNORE) {
				ignored.add(hash.hashCode());
			}
			else {
				secondary.add(hash.hashCode());
				if (score > bestScore) {
					bestDistance = score / bestScore;
					bestScore = score;
					best = hash.hashCode();
				}
			}
		}

		if (bestDistance > PRIMARY_PERSONALITY_DISTANCE) {
			primary = best;
			secondary.remove(best);
		}
		return new AjrsMultiplePersonality(type, primary, secondary, ignored);
	}

	private static double countScore(Map<Integer, Integer> input, Map<Integer, Integer> output) {
		Set<Integer> fields = new HashSet<Integer>(input.keySet());
		fields.addAll(output.keySet());

		int fc = fields.size();
		double totalScore = 0;
		for (Integer field : fields) {
			final boolean in = input.containsKey(field);
			final boolean out = output.containsKey(field);
			final int inCount = getCount(field, in, false, input, output);
			final int outCount = getCount(field, false, out, input, output);
			double score = (inCount * SCORE_FOR_INPUT) + (outCount * SCORE_FOR_OUTPUT);
			totalScore += score;
		}
		totalScore *= fc;
		return totalScore;
	}

	private static int getCount(Integer field, boolean in, boolean out, Map<Integer, Integer> input, Map<Integer, Integer> output) {
		int count = 0;
		if (in) {
			count += input.get(field);
		}
		if (out) {
			count += output.get(field);
		}
		return count;
	}
}
