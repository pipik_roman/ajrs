/**
 * 
 */
package ajrs.data.smells.duplicatedCode;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

import ajrs.data.AjrsObject;
import ajrs.data.smells.AjrsSmellCandidate;

/**
 * Do hash ulozime cestu ku kazdemu stromu ako index metody/statementu. Implementuje algoritmus v http://www.lsi.upc.edu/~valiente/graph-00-01-c.pdf
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsDuplicatedCode extends AjrsSmellCandidate {

	private final int size;
	protected final List<AjrsObject<? extends ASTNode>> duplicates;

	public AjrsDuplicatedCode(AjrsObject<? extends ASTNode> node, List<AjrsObject<? extends ASTNode>> duplicates, int size) {
		super(1, 1, DUPLICATED_CODE, node);
		this.size = size - 1;
		this.duplicates = duplicates;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Duplicated code (" + size + " duplicates with this) :\n");
		int index = 0;
		for (AjrsObject<? extends ASTNode> node : duplicates) {
			sb.append(index + ".----------------------------------------------------\n");
			sb.append(node.printPeer() + "\n");
			index++;
		}
		return sb.toString();
	}

	public List<AjrsObject<? extends ASTNode>> getDuplicates() {
		return duplicates;
	}
}
