/**
 * 
 */
package ajrs.data.smells.duplicatedCode;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.PackageDeclaration;

import ajrs.data.AjrsObject;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsAopDuplicatedCode extends AjrsDuplicatedCode {

	public AjrsAopDuplicatedCode(AjrsObject<? extends ASTNode> node, List<AjrsObject<? extends ASTNode>> duplicates, int size) {
		super(node, duplicates, size);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Duplicated code for AOP refactoring: \n");
		int index = 0;
		for (AjrsObject<? extends ASTNode> node : duplicates) {
			sb.append(index + ".----------------------------------------------------\n");
			sb.append(node.printPeer() + "\n");
			index++;
		}
		return sb.toString();
	}

	public String getPackage() {
		PackageDeclaration pd = getNode().getAjrsUnit().getPeer().getPackage();
		if (pd == null) {
			return "";
		} else {
			return pd.getName().getFullyQualifiedName();
		}
	}

}
