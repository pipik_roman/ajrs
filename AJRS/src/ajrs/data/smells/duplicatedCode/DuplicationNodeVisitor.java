/**
 * 
 */
package ajrs.data.smells.duplicatedCode;

/**
 * Visitor pre {@link DuplicationNode}
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public abstract interface DuplicationNodeVisitor {

	/**
	 * Navstivi uzol
	 * 
	 * @param node
	 *            uzol na navstivenie
	 * @return true ak ma pokracovat do child uzlov aktualneho uzla
	 */
	public boolean visit(DuplicationNodeModel node);
}
