/**
 * 
 */
package ajrs.data.smells.duplicatedCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jdt.core.dom.ASTNode;

import ajrs.Activator;
import ajrs.actions.ToggleShowDuplicationDataState;
import ajrs.data.AjrsObject;
import ajrs.data.AjrsUnit;
import ajrs.data.statement.AjrsBreakContinueStatement;
import ajrs.data.statement.AjrsReturnStatement;
import ajrs.data.statement.AjrsStatement;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsDuplicationAnalysis {

	/**
	 * Zoznam uzlov na spracovanie (q)
	 */
	private static final List<DuplicationNodeModel> queue = new ArrayList<DuplicationNodeModel>();
	/**
	 * Mapovanie Hash -> Pattern
	 */
	private static Map<Long, Integer> hashToPattern = new HashMap<Long, Integer>();
	/**
	 * Pocitadlo count
	 */
	private static int count = 0;

	private Map<Integer, List<DuplicationNodeModel>> duplicationData = null;

	public void analyse(AjrsUnit ajrsUnit) {
		DuplicationNode root = new DuplicationNode(ajrsUnit);
		isomorhism(root);
		IsomorphismDataCollector collector = new IsomorphismDataCollector();
		root.accept(collector);
		duplicationData = collector.patternToNodes;
		filterSubtrees();
		printResults();
		root.accept(new IsomorphismPrinter());
	}

	private void filterSubtrees() {
		List<Integer> integerToRemove = new ArrayList<Integer>();

		for (Integer index : duplicationData.keySet()) {
			List<DuplicationNodeModel> nodes = duplicationData.get(index);
			if (nodes.size() < getMinimalDuplication()) {
				integerToRemove.add(index);
				continue;
			}
			for (int i = 0; i < nodes.size(); i++) {
				DuplicationNodeModel node = nodes.get(i);
				AjrsObject<? extends ASTNode> peer = node.getPeer();
				if (ignoreForDuplication(peer)) {
					nodes.remove(i);
					i--;
				} else {
					DuplicationNodeModel parent = node.getParent();
					if (parent != null) {
						if (isIdentified(parent)) {
							nodes.remove(i);
							i--;
						}
					}
				}
			}
			if (nodes.size() < getMinimalDuplication()) {
				integerToRemove.add(index);
				continue;
			}
		}
		for (Integer integer : integerToRemove) {
			duplicationData.remove(integer);
		}
	}

	private boolean ignoreForDuplication(AjrsObject<? extends ASTNode> peer) {
		if (peer instanceof AjrsReturnStatement) {
			return true;
		}
		if (peer instanceof AjrsBreakContinueStatement) {
			return true;
		}
		return false;
	}

	private boolean isIdentified(DuplicationNodeModel parent) {
		int count = getCount(parent.getPattern());
		return count >= getMinimalDuplication();
	}

	private int getMinimalDuplication() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.DUPLICATION_MINIMUM);
	}

	public int getCount(int integer) {
		return duplicationData.get(integer).size();
	}

	protected void printResults() {
		for (Entry<Integer, List<DuplicationNodeModel>> entry : getDuplicationData().entrySet()) {
			System.err.println("Vzor: " + entry.getKey() + ", pocet: " + entry.getValue().size());
			System.err.println("Uzly: " + entry.getValue());
		}
	}

	/**
	 * @return the duplicationData duplication data Map duplicationSignature -> list of nodes with this signature
	 */
	public Map<Integer, List<DuplicationNodeModel>> getDuplicationData() {
		if (duplicationData == null) {
			throw new IllegalStateException("Duplication data are not present. Is this invoked before Duplication analysis is run?");
		}
		return duplicationData;
	}

	private void isomorhism(DuplicationNode root) {
		queue.clear(); // start with empty queue
		hashToPattern.clear();// do not clear to remember patterns!
		count = 0;
		addLeafsToQueue(root);
		while (!queue.isEmpty()) {
			processNodeFromQueue();
		}
	}

	private static void processNodeFromQueue() {
		DuplicationNodeModel node = queue.remove(0);
		if (!(node.getPeer() instanceof AjrsStatement)) {
			return;
		}
		assignPatternToSubtreeAt(node);

		DuplicationNodeModel parent = node.getParent();
		parent.incrementSize(node.getSize());
		parent.decrementChildren();

		if (ToggleShowDuplicationDataState.getStateValue()) {
			AjrsStatement statement = (AjrsStatement) node.getPeer();
			statement.addLineMarker("Pattern: " + node.getPattern() + ", duplication hash: " + Long.toHexString(node.getLabelWithChildren()).toUpperCase(), statement.getPeer().getStartPosition(),
					IMarker.SEVERITY_INFO, true);
		}
		if (parent.getChildren() == 0) {
			queue.add(parent); // add parent to queue if node is last children
		}
	}

	private static void assignPatternToSubtreeAt(DuplicationNodeModel node) {
		long l = node.getLabelWithChildren();
		Integer pattern = hashToPattern.get(l);
		if (pattern == null) {// not found
			count++;
			hashToPattern.put(l, count);
			node.setPattern(count);// new pattern number
		} else {
			node.setPattern(pattern);// same pattern number
		}
	}

	private static void addLeafsToQueue(DuplicationNode root) {
		root.accept(new DuplicationNodeVisitor() {

			@Override
			public boolean visit(DuplicationNodeModel node) {
				if (node.getChildren() == 0) {
					queue.add(node);
				}
				return true;
			}
		});
	}
}

class IsomorphismDataCollector implements DuplicationNodeVisitor {
	final Map<Integer, List<DuplicationNodeModel>> patternToNodes = new HashMap<Integer, List<DuplicationNodeModel>>();

	@Override
	public boolean visit(DuplicationNodeModel node) {
		int pattern = node.getPattern();
		List<DuplicationNodeModel> nodes = patternToNodes.get(pattern);
		if (nodes == null) {
			nodes = new ArrayList<DuplicationNodeModel>();
			patternToNodes.put(pattern, nodes);
		}
		nodes.add(node);
		return true;
	}
}

/**
 * Visitor for printing duplication tree data
 */
class IsomorphismPrinter implements DuplicationNodeVisitor {
	private final int depth;

	public IsomorphismPrinter() {
		this.depth = 0;
		System.out.println("Isomorphism: ");
	}

	public IsomorphismPrinter(int depth) {
		this.depth = depth;
	}

	@Override
	public boolean visit(DuplicationNodeModel node) {
		StringBuilder sb = new StringBuilder(depth * 2);
		for (int i = 0; i < depth; i++) {
			sb.append("|-");
		}
		System.out.println(sb.toString() + node.getLabel() + "[size=" + node.getSize() + ", integer=" + node.getPattern() + "+, label=" + node.getLabel() + "], " + node.getPeer().getDescription());
		List<DuplicationNodeModel> children = node.getChildrenList();
		if (children != null) {
			int newDepth = depth + 1;
			for (DuplicationNodeModel child : children) {
				child.accept(new IsomorphismPrinter(newDepth));
			}
		}
		return false;
	}
}

class NodeCounterVisitor implements DuplicationNodeVisitor {

	public NodeCounterVisitor() {

	}

	int count = 0;

	@Override
	public boolean visit(DuplicationNodeModel node) {
		count++;
		return true;
	}

	public int getTotal() {
		return count;
	}
}
