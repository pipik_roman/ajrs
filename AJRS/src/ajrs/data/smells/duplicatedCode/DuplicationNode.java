/**
 * 
 */
package ajrs.data.smells.duplicatedCode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

import ajrs.data.AjrsMethod;
import ajrs.data.AjrsObject;
import ajrs.data.AjrsType;
import ajrs.data.AjrsUnit;
import ajrs.data.statement.AjrsMethodBlock;
import ajrs.data.statement.AjrsNonLeafStatement;
import ajrs.data.statement.AjrsStatement;

/**
 * Implementacia uzla v stromovej strukture definovanej v http://www.lsi.upc.edu/~valiente/graph-00-01-c.pdf
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class DuplicationNode implements DuplicationNodeModel {
	private final List<DuplicationNodeModel> childrenList = new ArrayList<DuplicationNodeModel>();
	private final DuplicationNodeModel parent;
	private final AjrsObject<? extends ASTNode> peer;
	private int size = 1;
	private int children;
	private int pattern;

	public DuplicationNode(AjrsUnit unit) {
		this.parent = null;
		this.peer = unit;

		List<AjrsType> types = unit.getTypes();
		for (AjrsType type : types) {
			Collection<AjrsMethod> methods = type.getMethods().values();
			for (AjrsMethod method : methods) {
				AjrsMethodBlock block = method.getBlock();
				if (block != null) {
					DuplicationNode child = new DuplicationNode(this, block);
					this.childrenList.add(child);
				}
			}
		}
	}

	public DuplicationNode(DuplicationNodeModel parent, AjrsStatement peer) {
		this.parent = parent;
		this.peer = peer;
		if (peer.isLeaf()) {
			children = 0;
		} else {
			List<AjrsStatement> statements = ((AjrsNonLeafStatement) peer).getStatements();
			for (AjrsStatement statement : statements) {
				DuplicationNode child = new DuplicationNode(this, statement);
				this.childrenList.add(child);
			}
			children = this.childrenList.size();
		}

	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public void incrementSize(int size) {
		this.size += size;
	}

	@Override
	public int getChildren() {
		return children;
	}

	@Override
	public void decrementChildren() {
		children--;
	}

	@Override
	public int getPattern() {
		return pattern;
	}

	@Override
	public void setPattern(int pattern) {
		this.pattern = pattern;
	}

	@Override
	public int getLabel() {
		return peer.hashCode();
	}

	@Override
	public long getLabelWithChildren() {
		long l = getLabel();
		for (DuplicationNodeModel child : getChildrenList()) {
			l += child.getPattern();
		}
		return l;
	}

	@Override
	public List<DuplicationNodeModel> getChildrenList() {
		return childrenList;
	}

	@Override
	public DuplicationNodeModel getParent() {
		return parent;
	}

	@Override
	public int getTotalStatementCount() {
		int total = 1;
		for (DuplicationNodeModel child : childrenList) {
			total += child.getTotalStatementCount();
		}
		return total;
	}

	@Override
	public void accept(DuplicationNodeVisitor visitor) {
		if (visitor.visit(this)) {
			for (DuplicationNodeModel child : this.childrenList) {
				child.accept(visitor);
			}
		}
	}

	@Override
	public boolean isRoot() {
		return parent == null;
	}

	@Override
	public AjrsObject<? extends ASTNode> getPeer() {
		return peer;
	}

	@Override
	public String toString() {
		return peer.toString();
	}

	@Override
	public int hashCode() {
		return pattern;
	}
}
