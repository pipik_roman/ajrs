/**
 * 
 */
package ajrs.data.smells.duplicatedCode;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

import ajrs.data.AjrsObject;

/**
 * Model zodpoveda uzlu v stromovej strukture definovanej v http://www.lsi.upc.edu/~valiente/graph-00-01-c.pdf, plus obsahuje nejake pridane methody
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface DuplicationNodeModel {

	public int getSize();

	public void incrementSize(int size);

	public int getChildren();

	public void decrementChildren();

	public int getPattern();

	public void setPattern(int integer);

	public int getLabel();

	public long getLabelWithChildren();

	public List<DuplicationNodeModel> getChildrenList();

	public DuplicationNodeModel getParent();

	public int getTotalStatementCount();

	/**
	 * Visitor pattern. Prejde cez tento uzol a vsetky poduzly
	 * 
	 * @param visitor
	 *            visitor ktory sa ma prijat
	 */
	public void accept(DuplicationNodeVisitor visitor);

	/**
	 * @return true ak to je root
	 */
	public boolean isRoot();

	public AjrsObject<? extends ASTNode> getPeer();
}
