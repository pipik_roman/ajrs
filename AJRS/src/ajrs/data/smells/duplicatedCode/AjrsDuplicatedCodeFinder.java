/**
 * 
 */
package ajrs.data.smells.duplicatedCode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTNode;

import ajrs.data.AjrsObject;
import ajrs.data.AjrsUnit;
import ajrs.data.smells.AjrsSmellFinder;
import ajrs.data.statement.AjrsStatement;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsDuplicatedCodeFinder extends AjrsSmellFinder<AjrsDuplicatedCode> {
	private final List<AjrsDuplicatedCode> smells = new ArrayList<AjrsDuplicatedCode>();

	public AjrsDuplicatedCodeFinder() {

	}

	@Override
	public List<AjrsDuplicatedCode> find(AjrsUnit unit) {
		smells.clear();
		Map<Integer, List<DuplicationNodeModel>> data = unit.getDuplicationAnalysis().getDuplicationData();
		for (List<DuplicationNodeModel> nodes : data.values()) {
			int size = nodes.size();
			List<AjrsObject<? extends ASTNode>> peers = new ArrayList<AjrsObject<? extends ASTNode>>();
			for (DuplicationNodeModel node : nodes) {
				peers.add(node.getPeer());
			}

			for (DuplicationNodeModel node : nodes) {
				System.err.println("Node: " + node.getPeer().getClass() + ", " + node.getPeer());
			}
			for (DuplicationNodeModel node : nodes) {
				AjrsStatement statement = (AjrsStatement) node.getPeer();
				final AjrsDuplicatedCode smell;
				if (statement.isAopRefactoringPossible()) {
					smell = new AjrsAopDuplicatedCode(node.getPeer(), peers, size);
				} else if (!statement.isLeaf()) {
					smell = new AjrsDuplicatedCode(node.getPeer(), peers, size);
				} else {
					smell = null;
				}

				if (smell != null) {
					this.smells.add(smell);
				}
			}
		}
		return smells;
	}
}
