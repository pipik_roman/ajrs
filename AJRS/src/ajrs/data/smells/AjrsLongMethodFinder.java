/**
 * 
 */
package ajrs.data.smells;

import java.util.ArrayList;
import java.util.List;

import ajrs.Activator;
import ajrs.data.AjrsMethod;
import ajrs.data.AjrsUnit;
import ajrs.data.statement.AjrsBodyStatement;
import ajrs.data.statement.AjrsBreakContinueStatement;
import ajrs.data.statement.AjrsConditionalStatement;
import ajrs.data.statement.AjrsExpressionStatement;
import ajrs.data.statement.AjrsLoopStatement;
import ajrs.data.statement.AjrsMethodBlock;
import ajrs.data.statement.AjrsReturnStatement;
import ajrs.data.statement.AjrsThrowStatement;
import ajrs.data.statement.AjrsTryStatement;
import ajrs.data.statement.AjrsVariableStatement;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsLongMethodFinder extends AjrsSmellFinder<AjrsLongMethod> {
	private final List<AjrsLongMethod> smells = new ArrayList<AjrsLongMethod>();

	int size;

	@Override
	public List<AjrsLongMethod> find(AjrsUnit unit) {
		smells.clear();
		unit.accept(this);
		return smells;
	}

	@Override
	public boolean visit(AjrsMethod method) {
		size = 0;
		return true;
	}

	@Override
	public void afterVisit(AjrsMethod method) {
		if (size > getSizeLimit()) {
			smells.add(new AjrsLongMethod(method));
		}
		size = 0;
	}

	private int getSizeLimit() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.LONG_METHOD_SIZE_LIMIT);
	}

	@Override
	public boolean visit(AjrsBodyStatement statement) {
		return true;
	}

	@Override
	public void visit(AjrsBreakContinueStatement statement) {
		size++;
	}

	@Override
	public boolean visit(AjrsConditionalStatement statement) {
		size++;
		return true;
	}

	@Override
	public void visit(AjrsExpressionStatement statement) {
		size++;
	}

	@Override
	public boolean visit(AjrsLoopStatement statement) {
		size++;
		return true;
	}

	@Override
	public boolean visit(AjrsMethodBlock statement) {
		return true;
	}

	@Override
	public void visit(AjrsReturnStatement statement) {
		size++;
	}

	@Override
	public void visit(AjrsThrowStatement statement) {
		size++;
	}

	@Override
	public boolean visit(AjrsTryStatement statement) {
		size++;
		return true;
	}

	@Override
	public void visit(AjrsVariableStatement statement) {
		size++;
	}

}
