/**
 * 
 */
package ajrs.data.smells;

import java.util.ArrayList;
import java.util.List;

import ajrs.Activator;
import ajrs.data.AjrsUnit;
import ajrs.data.statement.AjrsBodyStatement;
import ajrs.data.statement.AjrsBreakContinueStatement;
import ajrs.data.statement.AjrsConditionalStatement;
import ajrs.data.statement.AjrsExpressionStatement;
import ajrs.data.statement.AjrsLeafStatement;
import ajrs.data.statement.AjrsLoopStatement;
import ajrs.data.statement.AjrsMethodBlock;
import ajrs.data.statement.AjrsNonLeafStatement;
import ajrs.data.statement.AjrsReturnStatement;
import ajrs.data.statement.AjrsStatement;
import ajrs.data.statement.AjrsThrowStatement;
import ajrs.data.statement.AjrsTryStatement;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsDepthComplexityFinder extends AjrsSmellFinder<AjrsDepthComplexity> {

	private static final Integer PROPERTY_SCORE = "DEPTH_COMPLEXITY_SCORE".hashCode();

	// zmenit na map aby sme mohli kontinualne vyhladav pachy a mali pre kazdy uzol maximalne jeden najdeny pach
	private List<AjrsDepthComplexity> smells = new ArrayList<AjrsDepthComplexity>();

	public AjrsDepthComplexityFinder() {

	}

	@Override
	public List<AjrsDepthComplexity> find(AjrsUnit unit) {
		this.smells.clear();
		unit.accept(this);
		return smells;
	}

	@Override
	public boolean visit(AjrsBodyStatement statement) {
		if (processStatement(statement)) { return false; }
		return true;
	}

	private double getScoreToIdentify() {
		return Activator.getDefault().getPreferenceStore().getDouble(PreferenceConstants.DEPTH_COMPLEXITY_LIMIT);
	}

	private void processStatement(AjrsLeafStatement statement) {
		int score = updateStatementScore(statement);
		if (score > getScoreToIdentify()) {
			smells.add(new AjrsDepthComplexity(statement));
		}
	}

	/**
	 * @param statement
	 * @return true ak bol najdeny pach. V takom pripade nejdeme viac do hlbky
	 */
	private boolean processStatement(AjrsNonLeafStatement statement) {
		int score = updateStatementScore(statement);

		if (score > getScoreToIdentify()) {
			smells.add(new AjrsDepthComplexity(statement));
			return true;
		}
		return false;
	}

	/**
	 * @param statement
	 */
	private int updateStatementScore(AjrsStatement statement) {
		Integer parentScore = (Integer) statement.getParent().getProperty(PROPERTY_SCORE);
		if (parentScore == null) {
			parentScore = 0;
		}
		int newScore = parentScore + statement.getComplexity();
		statement.setProperty(PROPERTY_SCORE, newScore);
		return newScore;
	}

	@Override
	public void visit(AjrsBreakContinueStatement statement) {
		processStatement(statement);
	}

	@Override
	public boolean visit(AjrsConditionalStatement statement) {
		if (processStatement(statement)) { return false; }
		return true;
	}

	@Override
	public void visit(AjrsExpressionStatement statement) {
		processStatement(statement);
	}

	@Override
	public boolean visit(AjrsLoopStatement statement) {
		if (processStatement(statement)) { return false; }
		return true;
	}

	@Override
	public boolean visit(AjrsMethodBlock statement) {
		if (processStatement(statement)) { return false; }
		return true;
	}

	@Override
	public void visit(AjrsReturnStatement statement) {
		processStatement(statement);
	}

	@Override
	public void visit(AjrsThrowStatement statement) {
		processStatement(statement);
	}

	@Override
	public boolean visit(AjrsTryStatement statement) {
		if (processStatement(statement)) { return false; }
		return true;
	}
}
