/**
 * 
 */
package ajrs.data.smells;

import ajrs.data.statement.AjrsConditionalStatement;

/**
 * Pach špeciálneho prípadu - if alebo switch z dlhým telom
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsSpecialCase extends AjrsSmellCandidate {

	public AjrsSpecialCase(AjrsConditionalStatement conditional) {
		super(2, 2, SPECIAL_CASE, conditional);
	}

	@Override
	public String toString() {
		return "Code in conditional statement should be short";
	}
}
