/**
 * 
 */
package ajrs.data.smells;

import java.util.ArrayList;
import java.util.List;

import ajrs.Activator;
import ajrs.data.AjrsField;
import ajrs.data.AjrsMethod;
import ajrs.data.AjrsType;
import ajrs.data.AjrsUnit;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsLazyClassFinder extends AjrsSmellFinder<AjrsLazyClass> {

	private final List<AjrsLazyClass> smells = new ArrayList<AjrsLazyClass>();
	private int fieldCount = 0;
	private int methodCount = 0;

	@Override
	public List<AjrsLazyClass> find(AjrsUnit unit) {
		smells.clear();
		unit.accept(this);
		return smells;
	}

	@Override
	public boolean visit(AjrsType type) {
		fieldCount = 0;
		methodCount = 0;
		return true;
	}

	@Override
	public boolean visit(AjrsMethod method) {
		methodCount++;
		return false;
	}

	@Override
	public void visit(AjrsField field) {
		fieldCount++;
	}

	@Override
	public void afterVisit(AjrsType type) {
		if (type.getPeer().resolveBinding().isInterface()) { return; }
		if (fieldCount < getFieldLimit() && methodCount < getMethodLimit()) {
			AjrsLazyClass smell = new AjrsLazyClass(type);
			this.smells.add(smell);
		}
	}

	private int getMethodLimit() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.LAZY_CLASS_METHOD_LIMIT);
	}

	private int getFieldLimit() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.LAZY_CLASS_FIELD_LIMIT);
	}

}
