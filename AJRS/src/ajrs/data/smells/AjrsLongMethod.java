/**
 * 
 */
package ajrs.data.smells;

import org.eclipse.jdt.core.dom.ASTNode;

import ajrs.data.AjrsObject;

/**
 * Pach dlhej metódy - podľa počtu statementov
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsLongMethod extends AjrsSmellCandidate {

	public AjrsLongMethod(AjrsObject<? extends ASTNode> node) {
		super(2, 2, LONG_METHOD, node);
	}

	@Override
	public String toString() {
		return "This method is too long!";
	}
}
