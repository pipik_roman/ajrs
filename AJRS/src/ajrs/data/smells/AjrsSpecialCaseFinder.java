/**
 * 
 */
package ajrs.data.smells;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SwitchStatement;

import ajrs.Activator;
import ajrs.data.AjrsMethod;
import ajrs.data.AjrsUnit;
import ajrs.data.statement.AjrsBodyStatement;
import ajrs.data.statement.AjrsBreakContinueStatement;
import ajrs.data.statement.AjrsConditionalStatement;
import ajrs.data.statement.AjrsExpressionStatement;
import ajrs.data.statement.AjrsLoopStatement;
import ajrs.data.statement.AjrsMethodBlock;
import ajrs.data.statement.AjrsReturnStatement;
import ajrs.data.statement.AjrsThrowStatement;
import ajrs.data.statement.AjrsTryStatement;
import ajrs.data.statement.AjrsVariableStatement;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsSpecialCaseFinder extends AjrsSmellFinder<AjrsSpecialCase> {

	private static final Integer PROPERTY_SCORE = "SPECIAL_CASE_SCORE".hashCode();

	private final List<AjrsSpecialCase> smells = new ArrayList<AjrsSpecialCase>();
	private final List<AjrsConditionalStatement> stack = new ArrayList<AjrsConditionalStatement>();

	@Override
	public List<AjrsSpecialCase> find(AjrsUnit unit) {
		smells.clear();
		unit.accept(this);
		return smells;
	}

	@Override
	public boolean visit(AjrsMethod method) {
		return true;
	}

	private int getSwitchSizeLimit() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.SWITCH_SIZE_LIMIT);
	}

	private int getIfSizeLimit() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.IF_SIZE_LIMIT);
	}

	@Override
	public boolean visit(AjrsBodyStatement statement) {
		return true;
	}

	@Override
	public void visit(AjrsBreakContinueStatement statement) {
		increment();
	}

	private void increment() {
		if (stack.isEmpty()) { return; }
		AjrsConditionalStatement conditional = stack.get(stack.size() - 1);
		Integer score = (Integer) conditional.getProperty(PROPERTY_SCORE);
		if (score == null) {
			score = 0;
		}
		score++;
		conditional.setProperty(PROPERTY_SCORE, score);
	}

	@Override
	public boolean visit(AjrsConditionalStatement statement) {
		increment();
		stack.add(statement);
		return true;
	}

	@Override
	public void afterVisit(AjrsConditionalStatement statement) {
		Statement s = statement.getPeer();
		final int limit;
		if (s instanceof SwitchStatement) {
			limit = getSwitchSizeLimit();
		}
		else if (s instanceof IfStatement) {
			limit = getIfSizeLimit();
		}
		else {
			throw new AssertionError();
		}
		Integer score = (Integer) statement.getProperty(PROPERTY_SCORE);
		if (score == null) { return; }
		if (score > limit) {
			smells.add(new AjrsSpecialCase(statement));
		}
		stack.remove(stack.size() - 1);

		if (stack.isEmpty()) {
			return;
		}
		else {
			AjrsConditionalStatement parent = stack.get(stack.size() - 1);
			parent.setProperty(PROPERTY_SCORE, ((Integer) parent.getProperty(PROPERTY_SCORE)) + score);
		}
	}

	@Override
	public void visit(AjrsExpressionStatement statement) {
		increment();
	}

	@Override
	public boolean visit(AjrsLoopStatement statement) {
		increment();
		return true;
	}

	@Override
	public boolean visit(AjrsMethodBlock statement) {
		return true;
	}

	@Override
	public void visit(AjrsReturnStatement statement) {
		increment();
	}

	@Override
	public void visit(AjrsThrowStatement statement) {
		increment();
	}

	@Override
	public boolean visit(AjrsTryStatement statement) {
		increment();
		return true;
	}

	@Override
	public void visit(AjrsVariableStatement statement) {
		increment();
	}

}
