/**
 * 
 */
package ajrs.data.smells;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

import ajrs.Activator;
import ajrs.data.AjrsNode;
import ajrs.data.AjrsUnit;
import ajrs.data.statement.AjrsBodyStatement;
import ajrs.data.statement.AjrsBreakContinueStatement;
import ajrs.data.statement.AjrsConditionalStatement;
import ajrs.data.statement.AjrsExpressionStatement;
import ajrs.data.statement.AjrsLoopStatement;
import ajrs.data.statement.AjrsMethodBlock;
import ajrs.data.statement.AjrsReturnStatement;
import ajrs.data.statement.AjrsStatement;
import ajrs.data.statement.AjrsThrowStatement;
import ajrs.data.statement.AjrsTryStatement;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsSizeComplexityFinder extends AjrsSmellFinder<AjrsSizeComplexity> {
	private static final Integer PROPERTY_SCORE = "SIZE_COMPLEXITY_SCORE".hashCode();

	// zmenit na map aby sme mohli kontinualne vyhladav pachy a mali pre kazdy uzol maximalne jeden najdeny pach
	private List<AjrsSizeComplexity> smells = new ArrayList<AjrsSizeComplexity>();

	@Override
	public List<AjrsSizeComplexity> find(AjrsUnit unit) {
		this.smells.clear();
		unit.accept(this);
		return smells;
	}

	@Override
	public boolean visit(AjrsBodyStatement statement) {
		if (processStatement(statement)) { return false; }
		return true;
	}

	public int getInputCoeficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.INPUT_VARIABLE_COEF);
	}

	public int getOutputCoeficient() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.OUTPUT_VARIABLE_COEF);
	}

	private double getSizeLimit() {
		return Activator.getDefault().getPreferenceStore().getDouble(PreferenceConstants.SIZE_COMPLEXITY_LIMIT);
	}

	/**
	 * @param statement
	 * @return true ak bol najdeny pach. V takom pripade nejdeme viac do hlbky
	 */
	private boolean processStatement(AjrsStatement statement) {
		AjrsNode<? extends ASTNode> parent = statement.getParent();
		if (parent != null && parent instanceof AjrsStatement) {
			Integer score = (Integer) parent.getProperty(PROPERTY_SCORE);
			if (score == null) {
				score = 0;
				parent.setProperty(PROPERTY_SCORE, score);
			}
			// ak uz parent ma velke skore tak uz bol oznaceny
			if (score > getSizeLimit()) { return true; }

			score = updateStatementScore(statement);
			if (score > getSizeLimit()) {
				smells.add(new AjrsSizeComplexity(statement.getParent()));
				return true;
			}
		}

		return false;
	}

	private int updateStatementScore(AjrsStatement statement) {
		AjrsNode<? extends ASTNode> parent = statement.getParent();
		Integer parentScore = (Integer) parent.getProperty(PROPERTY_SCORE);

		int newScore = parentScore + statement.getComplexity();
		parent.setProperty(PROPERTY_SCORE, newScore);
		return newScore;
	}

	@Override
	public void visit(AjrsBreakContinueStatement statement) {
		processStatement(statement);
	}

	@Override
	public boolean visit(AjrsConditionalStatement statement) {
		if (processStatement(statement)) { return false; }
		return true;
	}

	@Override
	public void visit(AjrsExpressionStatement statement) {
		processStatement(statement);
	}

	@Override
	public boolean visit(AjrsLoopStatement statement) {
		if (processStatement(statement)) { return false; }
		return true;
	}

	@Override
	public boolean visit(AjrsMethodBlock statement) {
		if (processStatement(statement)) { return false; }
		return true;
	}

	@Override
	public void visit(AjrsReturnStatement statement) {
		processStatement(statement);
	}

	@Override
	public void visit(AjrsThrowStatement statement) {
		processStatement(statement);
	}

	@Override
	public boolean visit(AjrsTryStatement statement) {
		if (processStatement(statement)) { return false; }
		return true;
	}
}
