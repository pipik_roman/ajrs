/**
 * 
 */
package ajrs.data.smells;

import ajrs.data.AjrsType;

/**
 * Pach malej triedy - má málo metód alebo polí
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsLazyClass extends AjrsSmellCandidate {

	public AjrsLazyClass(AjrsType type) {
		super(2, 2, LAZY_CLASS, type);
	}

	@Override
	public String toString() {
		return "This class is too small. It should have more fields or methods!";
	}

}
