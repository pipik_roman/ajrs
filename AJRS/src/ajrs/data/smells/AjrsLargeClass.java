/**
 * 
 */
package ajrs.data.smells;

import ajrs.data.AjrsType;

/**
 * Pach pre veľkú triedu - ak má veľa metód alebo polí
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsLargeClass extends AjrsSmellCandidate {

	public AjrsLargeClass(AjrsType type) {
		super(2, 2, LARGE_CLASS, type);
	}

	@Override
	public String toString() {
		return "This class is too large, it has too many fields or too many methods!";
	}
}
