/**
 * 
 */
package ajrs.data.smells;

import ajrs.data.AjrsMethod;

/**
 * Pach dlhého počtu parametrov - podľa method declaration
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsTooManyArguments extends AjrsSmellCandidate {
	public AjrsTooManyArguments(AjrsMethod method) {
		super(2, 2, TOO_MANY_ARGUMENTS, method);
	}

	@Override
	public String toString() {
		return "Method has too many parameters";
	}
}
