/**
 * 
 */
package ajrs.data.smells;

import java.util.List;

import ajrs.data.AjrsObjectVisitor;
import ajrs.data.AjrsUnit;

/**
 * Tato trieda je abstrakt pre vsetky triedy vyhladavajuce pachy nad Ajrs modelom
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <SmellCandidate>
 *            typ kandidata pachu ktory tento finder najde
 */
public abstract class AjrsSmellFinder<SmellCandidate extends AjrsSmellCandidate> extends AjrsObjectVisitor {

	/**
	 * Najde vsetkych kandidatov na pach v AjrsUnit.
	 * 
	 * @param unit
	 *            korenovy uzol Ajrs modelu
	 * @return vsetkych najdenych kandidatov na pach
	 */
	public abstract List<SmellCandidate> find(AjrsUnit unit);
}
