/**
 * 
 */
package ajrs.data.smells;

import java.io.File;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;

import ajrs.data.AjrsHash;
import ajrs.data.AjrsObject;

/**
 * Todo vseobecne vlastnosti pre pach
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsSmellCandidate extends AjrsHash {

	protected static final int PART_TYPE = 0;
	protected static final int DEPTH_COMPLEXITY = 0x10;
	protected static final int LARGE_CLASS = 0x20;
	protected static final int LAZY_CLASS = 0x30;
	protected static final int LONG_METHOD = 0x40;
	protected static final int MAGIC_NUMBER = 0x50;
	protected static final int MULTIPLE_PERSONALITY = 0x60;
	protected static final int SIZE_COMPLEXITY = 0x70;
	protected static final int SPECIAL_CASE = 0x80;
	protected static final int TOO_MANY_ARGUMENTS = 0x90;
	protected static final int DUPLICATED_CODE = 0xA0;

	private final AjrsObject<? extends ASTNode> node;

	public AjrsSmellCandidate(int uniquePart, int hashParts, int type, AjrsObject<? extends ASTNode> node) {
		super(uniquePart, hashParts);
		this.node = node;
		setPart(PART_TYPE, new int[] { type });
	}

	public AjrsObject<? extends ASTNode> getNode() {
		return node;
	}

	/**
	 * Returns package name for smell, or empty string if package is default
	 * 
	 * @return package name for smell, or empty string if package is default
	 */
	public File getFile() {
		CompilationUnit cu = node.getAjrsUnit().getPeer();
		return cu.getJavaElement().getPath().toFile();
	}

}
