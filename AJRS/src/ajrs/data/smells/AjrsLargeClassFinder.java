/**
 * 
 */
package ajrs.data.smells;

import java.util.ArrayList;
import java.util.List;

import ajrs.Activator;
import ajrs.data.AjrsField;
import ajrs.data.AjrsMethod;
import ajrs.data.AjrsType;
import ajrs.data.AjrsUnit;
import ajrs.preferences.PreferenceConstants;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsLargeClassFinder extends AjrsSmellFinder<AjrsLargeClass> {

	private final List<AjrsLargeClass> smells = new ArrayList<AjrsLargeClass>();
	private int fieldCount = 0;
	private int methodCount = 0;

	@Override
	public List<AjrsLargeClass> find(AjrsUnit unit) {
		smells.clear();
		unit.accept(this);
		return smells;
	}

	@Override
	public boolean visit(AjrsType type) {
		fieldCount = 0;
		methodCount = 0;

		return true;
	}

	@Override
	public boolean visit(AjrsMethod method) {
		methodCount++;
		return false;
	}

	@Override
	public void visit(AjrsField field) {
		fieldCount++;
	}

	@Override
	public void afterVisit(AjrsType type) {
		if (type.getPeer().resolveBinding().isInterface()) { return; }
		if (fieldCount > getFieldLimit() || methodCount > getMethodLimit()) {
			AjrsLargeClass smell = new AjrsLargeClass(type);
			this.smells.add(smell);
		}
	}

	private int getMethodLimit() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.LARGE_CLASS_METHOD_LIMIT);
	}

	private int getFieldLimit() {
		return Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.LARGE_CLASS_FIELD_LIMIT);
	}

}
