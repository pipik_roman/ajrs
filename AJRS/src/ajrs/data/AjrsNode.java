/**
 * 
 */
package ajrs.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.ArrayInitializer;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.SynchronizedStatement;
import org.eclipse.jdt.core.dom.ThisExpression;
import org.eclipse.jdt.core.dom.TypeLiteral;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

/**
 * Implementuje spolocnu logiku pre spracovanie statementov ako aj zavislosti metod. Metody aj statementy maju input a output dependency.
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <AstPeer>
 *            ast peer type
 */
@SuppressWarnings("unchecked")
public abstract class AjrsNode<AstPeer extends ASTNode> extends AjrsFailsafeDecorator<AstPeer> {

	/**
	 * Parameters part is second part of hash (after statement type)
	 */
	public static final int PART_PARAMETERS = 1;

	public static final int LOCAL_VARIABLE_HASH_OFFSET = "LOCAL_VARIABLE_HASH_OFFSET".hashCode();// 0xAAAAA0A0;
	public static final int PARAMETER_VARIABLE_HASH_OFFSET = "PARAMETER_VARIABLE_HASH_OFFSET".hashCode();// 0xAAAAA0A0;
	public static final int METHOD_INVOCATION_HASH_OFFSET = "METHOD_INVOCATION_HASH_OFFSET".hashCode();// 0xAAAAA0B0;
	private static final int INSTANCEOF = "INSTANCEOF".hashCode();// 0xAAAAA0C0;

	private static final int LITERAL_NULL = "LITERAL_NULL ".hashCode();// 0xAAAAA280;
	private static final int LITERAL_CHAR = "LITERAL_CHAR".hashCode();// 0xAAAAA290;
	private static final int LITERAL_NUMBER = "LITERAL_NUMBER".hashCode();// 0xAAAAA2A0;
	private static final int LITERAL_BOOLEAN = "LITERAL_BOOLEAN".hashCode();// 0xAAAAA2B0;
	private static final int LITERAL_STRING = "LITERAL_STRING".hashCode();// 0xAAAAA2C0;
	private static final int LITERAL_TYPE = "LITERAL_TYPE".hashCode();// 0xAAAAA2D0;

	private static final int INFIX_TIMES = "INFIX_TIMES".hashCode();// 0xAAAAA0D0;
	private static final int INFIX_DIVIDE = "INFIX_DIVIDE".hashCode();// 0xAAAAA0E0;
	private static final int INFIX_REMAINDER = "INFIX_REMAINDER".hashCode();// 0xAAAAA0F0;
	private static final int INFIX_PLUS = "INFIX_PLUS".hashCode();// 0xAAAAA100;
	private static final int INFIX_MINUS = "INFIX_MINUS".hashCode();// 0xAAAAA110;
	private static final int INFIX_LEFT_SHIFT = "INFIX_LEFT_SHIFT".hashCode();// 0xAAAAA120;
	private static final int INFIX_RIGHT_SHIFT_SIGNED = "INFIX_RIGHT_SHIFT_SIGNED".hashCode();// 0xAAAAA130;
	private static final int INFIX_RIGHT_SHIFT_UNSIGNED = "INFIX_RIGHT_SHIFT_UNSIGNED".hashCode();// 0xAAAAA140;
	private static final int INFIX_LESS = "INFIX_LESS".hashCode();// 0xAAAAA150;
	private static final int INFIX_GREATER = "INFIX_GREATER".hashCode();// 0xAAAAA160;
	private static final int INFIX_LESS_EQUALS = "INFIX_LESS_EQUALS".hashCode();// 0xAAAAA170;
	private static final int INFIX_GREATER_EQUAL = "INFIX_GREATER_EQUAL".hashCode();// 0xAAAAA180;
	private static final int INFIX_EQUALS = "INFIX_GREATER_EQUAL".hashCode();// 0xAAAAA190;
	private static final int INFIX_NOT_EQUALS = "INFIX_NOT_EQUALS".hashCode();// 0xAAAAA1A0;
	private static final int INFIX_XOR = "INFIX_XOR".hashCode();// 0xAAAAA1B0;
	private static final int INFIX_AND = "INFIX_AND".hashCode();// 0xAAAAA1C0;
	private static final int INFIX_OR = "INFIX_OR".hashCode();// 0xAAAAA1D0;
	private static final int INFIX_CONDITIONAL_AND = "INFIX_CONDITIONAL_AND".hashCode();// 0xAAAAA1E0;
	private static final int INFIX_CONDITIONAL_OR = "INFIX_CONDITIONAL_OR".hashCode();// 0xAAAAA1F0;
	private static final int PREFIX_INCREMENT = "INFIX_CONDITIONAL_OR".hashCode();// 0xAAAAA200;
	private static final int PREFIX_DECREMENT = "PREFIX_DECREMENT".hashCode();// 0xAAAAA210;
	private static final int PREFIX_PLUS = "PREFIX_PLUS".hashCode();// 0xAAAAA220;
	private static final int PREFIX_MINUS = "PREFIX_MINUS".hashCode();// 0xAAAAA230;
	private static final int PREFIX_COMPLEMENT = "PREFIX_COMPLEMENT".hashCode();// 0xAAAAA240;
	private static final int PREFIX_NOT = "PREFIX_NOT".hashCode();// 0xAAAAA250;
	private static final int POSTFIX_INCREMENT = "POSTFIX_INCREMENT".hashCode();// 0xAAAAA260;
	private static final int POSTFIX_DECREMENT = "POSTFIX_DECREMENT".hashCode();// 0xAAAAA270;

	private static final Map<Object, Integer> operatorToHashCode = new HashMap<Object, Integer>();
	static {
		operatorToHashCode.put(InfixExpression.Operator.AND, INFIX_AND);
		operatorToHashCode.put(InfixExpression.Operator.CONDITIONAL_AND, INFIX_CONDITIONAL_AND);
		operatorToHashCode.put(InfixExpression.Operator.CONDITIONAL_OR, INFIX_CONDITIONAL_OR);
		operatorToHashCode.put(InfixExpression.Operator.DIVIDE, INFIX_DIVIDE);
		operatorToHashCode.put(InfixExpression.Operator.EQUALS, INFIX_EQUALS);
		operatorToHashCode.put(InfixExpression.Operator.GREATER, INFIX_GREATER);
		operatorToHashCode.put(InfixExpression.Operator.GREATER_EQUALS, INFIX_GREATER_EQUAL);
		operatorToHashCode.put(InfixExpression.Operator.LEFT_SHIFT, INFIX_LEFT_SHIFT);
		operatorToHashCode.put(InfixExpression.Operator.LESS, INFIX_LESS);
		operatorToHashCode.put(InfixExpression.Operator.LESS_EQUALS, INFIX_LESS_EQUALS);
		operatorToHashCode.put(InfixExpression.Operator.MINUS, INFIX_MINUS);
		operatorToHashCode.put(InfixExpression.Operator.NOT_EQUALS, INFIX_NOT_EQUALS);
		operatorToHashCode.put(InfixExpression.Operator.OR, INFIX_OR);
		operatorToHashCode.put(InfixExpression.Operator.PLUS, INFIX_PLUS);
		operatorToHashCode.put(InfixExpression.Operator.REMAINDER, INFIX_REMAINDER);
		operatorToHashCode.put(InfixExpression.Operator.RIGHT_SHIFT_SIGNED, INFIX_RIGHT_SHIFT_SIGNED);
		operatorToHashCode.put(InfixExpression.Operator.RIGHT_SHIFT_UNSIGNED, INFIX_RIGHT_SHIFT_UNSIGNED);
		operatorToHashCode.put(InfixExpression.Operator.TIMES, INFIX_TIMES);
		operatorToHashCode.put(InfixExpression.Operator.XOR, INFIX_XOR);
		operatorToHashCode.put(PrefixExpression.Operator.COMPLEMENT, PREFIX_COMPLEMENT);
		operatorToHashCode.put(PrefixExpression.Operator.DECREMENT, PREFIX_DECREMENT);
		operatorToHashCode.put(PrefixExpression.Operator.INCREMENT, PREFIX_INCREMENT);
		operatorToHashCode.put(PrefixExpression.Operator.MINUS, PREFIX_MINUS);
		operatorToHashCode.put(PrefixExpression.Operator.NOT, PREFIX_NOT);
		operatorToHashCode.put(PrefixExpression.Operator.PLUS, PREFIX_PLUS);
		operatorToHashCode.put(PostfixExpression.Operator.DECREMENT, POSTFIX_DECREMENT);
		operatorToHashCode.put(PostfixExpression.Operator.INCREMENT, POSTFIX_INCREMENT);
		operatorToHashCode.put(Assignment.Operator.BIT_AND_ASSIGN, INFIX_AND);
		operatorToHashCode.put(Assignment.Operator.BIT_OR_ASSIGN, INFIX_OR);
		operatorToHashCode.put(Assignment.Operator.BIT_XOR_ASSIGN, INFIX_XOR);
		operatorToHashCode.put(Assignment.Operator.DIVIDE_ASSIGN, INFIX_DIVIDE);
		operatorToHashCode.put(Assignment.Operator.LEFT_SHIFT_ASSIGN, INFIX_LEFT_SHIFT);
		operatorToHashCode.put(Assignment.Operator.MINUS_ASSIGN, INFIX_MINUS);
		operatorToHashCode.put(Assignment.Operator.PLUS_ASSIGN, INFIX_PLUS);
		operatorToHashCode.put(Assignment.Operator.REMAINDER_ASSIGN, INFIX_REMAINDER);
		operatorToHashCode.put(Assignment.Operator.RIGHT_SHIFT_SIGNED_ASSIGN, INFIX_RIGHT_SHIFT_SIGNED);
		operatorToHashCode.put(Assignment.Operator.RIGHT_SHIFT_UNSIGNED_ASSIGN, INFIX_RIGHT_SHIFT_UNSIGNED);
		operatorToHashCode.put(Assignment.Operator.TIMES_ASSIGN, INFIX_TIMES);
	}
	private final Set<String> readFields = new LinkedHashSet<String>();
	private final Set<String> writeFields = new LinkedHashSet<String>();
	private final List<Object> literals = new ArrayList<Object>();

	protected final State state = new State();
	private final AjrsNodeData nodeData = new AjrsNodeData();
	private final boolean processNodeParameters;

	protected int fieldReferences = 0;
	protected int parameterReferences = 0;
	protected int localReferences = 0;

	/**
	 * @param uniqueParts
	 *            pocet casti hash ktore tvoria jeho identitu
	 * @param hashParts
	 *            number of hash parts
	 * @param parent
	 *            parent of this object, or null
	 * @param peer
	 *            peer object in ast
	 * @param processNodeParameters
	 *            if true, node parameters are processed
	 */
	public AjrsNode(int uniqueParts, int hashParts, AjrsObject<? extends ASTNode> parent, AstPeer peer, boolean processNodeParameters) {
		super(uniqueParts, hashParts, parent, peer);
		this.processNodeParameters = processNodeParameters;
	}

	/**
	 * Convert node data to hash
	 */
	protected abstract void nodeDataToHash();

	/**
	 * @return the nodeData
	 */
	public AjrsNodeData getNodeData() {
		return nodeData;
	}

	/**
	 * Returns true if method is in actual type
	 * 
	 * @param method
	 *            method to test
	 * @return true if method is in actual type
	 */
	protected boolean isInThisType(IMethodBinding method) {
		return isThisType(method.getDeclaringClass());
	}

	/**
	 * Returns true if variable is in actual type
	 * 
	 * @param variable
	 *            variable to test
	 * @return true if variable is in actual type
	 */
	protected boolean isInThisType(IVariableBinding variable) {
		return isThisType(variable.getDeclaringClass());
	}

	/**
	 * Returns true if type is actual type
	 * 
	 * @param type
	 *            type to test
	 * @return true if type is actual type
	 */
	protected boolean isThisType(ITypeBinding type) {
		if (type == null) {
			return false;
		}
		AjrsHash hash = AjrsType.createHash(type);
		AjrsType ajrsType = getType();
		return hash.equals(ajrsType.hash);
	}

	/**
	 * Vrati typ v ktorom je vnoreny tento visitor (Typ ktory dekladuje metodu a pod)
	 * 
	 * @return typ v ktorom je vnoreny tento visitor (Typ ktory dekladuje metodu a pod)
	 */
	public abstract AjrsType getType();

	/*----------------------------------------VSEOBECNE IMPLEMENTACIE SPOLOCNE PRE OBA DRUHY SPRACOVANIA---------------------------------*/

	@Override
	public boolean visit(InfixExpression node) {
		InfixExpression.Operator operator = node.getOperator();

		Expression left = node.getLeftOperand();
		left.accept(this);

		addOperatorParameter(operator);

		Expression right = node.getRightOperand();
		right.accept(this);

		List<Expression> operands = node.extendedOperands();
		for (Expression operand : operands) {
			operand.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(ArrayCreation node) {
		List<Expression> dimensions = node.dimensions();
		if (dimensions != null) {
			for (Expression e : dimensions) {
				e.accept(this);
			}
		}
		ArrayInitializer initializer = node.getInitializer();
		if (initializer != null) {
			initializer.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(ArrayInitializer node) {
		List<Expression> expressions = node.expressions();
		if (expressions != null) {
			for (Expression expression : expressions) {
				expression.accept(this);
			}
		}
		return false;
	}

	@Override
	public boolean visit(SynchronizedStatement node) {
		node.getBody().accept(this);
		return false;
	}

	private void addOperatorParameter(Object o) {
		if (!processNodeParameters) {
			return;
		}

		Integer hashCode = operatorToHashCode.get(o);
		if (hashCode == null) {
			throw new AssertionError("Unknown operator: " + o);
		}
		nodeData.addParameter(hashCode);
	}

	private void addLiteralParameter(Integer literal) {
		if (!processNodeParameters) {
			return;
		}
		nodeData.addParameter(literal);
	}

	@Override
	public boolean visit(VariableDeclarationExpression node) {
		List<VariableDeclarationFragment> fragments = node.fragments();
		for (VariableDeclarationFragment fragment : fragments) {
			fragment.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(VariableDeclarationFragment node) {

		state.setAssignmentLeft(true);
		node.getName().accept(this);
		state.setAssignmentLeft(false);

		Expression initializer = node.getInitializer();
		if (initializer != null) {
			initializer.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(PrefixExpression node) {
		PrefixExpression.Operator operator = node.getOperator();
		addOperatorParameter(operator);
		if (operator == PrefixExpression.Operator.INCREMENT || operator == PrefixExpression.Operator.DECREMENT) {
			Expression operand = node.getOperand();
			state.setAssignmentLeft(true);
			state.setAssignmentRight(true);
			operand.accept(this);
			state.setAssignmentLeft(false);
			state.setAssignmentRight(false);
		} else {
			Expression operand = node.getOperand();
			operand.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(SingleVariableDeclaration node) {
		Expression initializer = node.getInitializer();
		if (initializer != null) {
			initializer.accept(this);
		}
		state.setAssignmentLeft(true);
		node.getName().accept(this);
		state.setAssignmentLeft(false);
		return false;
	}

	@Override
	public boolean visit(MethodInvocation node) {
		Expression expression = node.getExpression();
		if (expression != null) {
			expression.accept(this);
		}
		node.getName().accept(this);

		List<Expression> arguments = node.arguments();
		for (Expression argument : arguments) {
			argument.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(FieldAccess node) {
		Expression expression = node.getExpression();
		expression.accept(this);
		SimpleName name = node.getName();
		name.accept(this);
		return false;
	}

	@Override
	public boolean visit(ClassInstanceCreation node) {
		List<Expression> arguments = node.arguments();
		IMethodBinding constructor = node.resolveConstructorBinding();
		processMethodBinding(constructor);

		Expression expression = node.getExpression();
		if (expression != null) {
			expression.accept(this);
		}

		if (processNodeParameters) {
			nodeData.addParameter(node.getType().resolveBinding().getQualifiedName().hashCode());
		}

		for (Expression argument : arguments) {
			argument.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(Assignment node) {
		assert !state.isLeft() : "This should not happen and is not supported. Assignment:  " + node + ", state: " + state;
		Assignment.Operator operator = node.getOperator();

		boolean notAssign = operator != Assignment.Operator.ASSIGN;
		if (notAssign) {
			// pre += a podobne mozeme brat lavu stranu ze je aj vpravo
			state.setAssignmentRight(true);
		}
		state.setAssignmentLeft(true);
		Expression left = node.getLeftHandSide();
		left.accept(this);
		state.setAssignmentLeft(false);

		if (notAssign) {
			state.setAssignmentRight(false);
			addOperatorParameter(operator);
		}

		state.setAssignmentRight(true);
		Expression right = node.getRightHandSide();
		right.accept(this);
		state.setAssignmentRight(false);
		return false;
	}

	@Override
	public boolean visit(QualifiedName node) {
		processName(node, false);
		return false;
	}

	@Override
	public boolean visit(PostfixExpression node) {
		PostfixExpression.Operator operator = node.getOperator();

		Expression operand = node.getOperand();
		state.setAssignmentLeft(true);
		state.setAssignmentRight(true);
		operand.accept(this);
		state.setAssignmentLeft(false);
		state.setAssignmentRight(false);

		addOperatorParameter(operator);
		return false;
	}

	@Override
	public boolean visit(ParenthesizedExpression node) {
		node.getExpression().accept(this);
		return false;
	}

	@Override
	public boolean visit(ConditionalExpression node) {
		node.getExpression().accept(this);
		node.getThenExpression().accept(this);
		node.getElseExpression().accept(this);
		return false;
	}

	@Override
	public boolean visit(InstanceofExpression node) {
		Expression left = node.getLeftOperand();
		left.accept(this);
		addInstanceofParameter();
		return false;
	}

	private void addInstanceofParameter() {
		if (!processNodeParameters) {
			return;
		}
		nodeData.addParameter(INSTANCEOF);
	}

	@Override
	public boolean visit(CastExpression node) {
		node.getExpression().accept(this);
		return false;
	}

	@Override
	public boolean visit(ConstructorInvocation node) {
		List<Expression> arguments = node.arguments();
		for (Expression argument : arguments) {
			argument.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(SuperMethodInvocation node) {
		List<Expression> expressions = node.arguments();

		node.getName().accept(this);

		for (Expression expression : expressions) {
			expression.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(ArrayAccess node) {
		node.getArray().accept(this);
		node.getIndex().accept(this);
		return false;
	}

	@Override
	public boolean visit(SimpleName node) {
		processName(node, node.isDeclaration());
		return false;
	}

	public void processName(Name node, boolean isDeclaration) {
		final IBinding binding = node.resolveBinding();
		if (binding == null) {
			System.err.println("Binding not resolved for: " + node);
			return;
		}
		if (binding instanceof IVariableBinding) {
			IVariableBinding variable = (IVariableBinding) binding;
			if (variable.isField()) {
				processField(variable, isDeclaration);
			} else if (variable.isParameter()) {
				processParameter(variable, isDeclaration);
			} else {
				processLocal(variable, isDeclaration);
			}
		} else if (binding instanceof IMethodBinding) {
			processMethodBinding((IMethodBinding) binding);
		} else {
			System.err.println("Unknown IBinding: " + binding.getClass() + ", " + binding.getName());
		}
	}

	public void addDependency(IVariableBinding variable, boolean local, boolean parameter) {
		final String name = variable.getName();
		Integer hash = name.hashCode();
		boolean isField = (!local) && (!parameter);
		if (local) {
			this.localReferences++;
			hash += LOCAL_VARIABLE_HASH_OFFSET;
		}
		if (parameter) {
			this.parameterReferences++;
			hash += PARAMETER_VARIABLE_HASH_OFFSET;
		}
		if (isField) {
			this.fieldReferences++;
		}
		System.err.println("variable: " + variable.getName() + ", local: " + local + ", field: " + isField + ", parameter: " + parameter + ", hash: " + hash);
		if (state.isLeft()) {// WRITE
			nodeData.addOutputDependency(hash);
			if (isField) {
				writeFields.add(name);
			}
		} else {// if (state.isInputDependencyState()) { // ak to nieje field write tak je to read
			nodeData.addInputDependency(hash);
			if (isField) {
				readFields.add(name);
			}
		}

		if (state.isRight()) {// osobitne lebo moze byt vlavo aj vpravo sucasne pre operatory += a podobne
			nodeData.addInputDependency(hash);
			if (isField) {
				readFields.add(name);
			}
		}
	}

	public int getFieldReferenceCount() {
		return this.fieldReferences;
	}

	public Set<AjrsField> getFields() {
		Set<AjrsField> fields = new LinkedHashSet<AjrsField>();
		fields.addAll(getReadFields());
		fields.addAll(getWriteFields());
		return fields;
	}

	public List<AjrsField> getReadFields() {
		List<AjrsField> fields = new ArrayList<AjrsField>();
		for (AjrsField field : getType().getFields()) {
			if (readFields.contains(field.getName())) {
				fields.add(field);
			}
		}
		return fields;
	}

	public List<AjrsField> getWriteFields() {
		List<AjrsField> fields = new ArrayList<AjrsField>();
		for (AjrsField field : getType().getFields()) {
			if (writeFields.contains(field.getName())) {
				fields.add(field);
			}
		}
		return fields;
	}

	public List<Object> getLiterals() {
		return literals;
	}

	public void processField(IVariableBinding variable, boolean isDeclaration) {
		if (!isInThisType(variable)) {
			return; // pre iné triedy neriešime
		}
		addDependency(variable, false, false);
	}

	protected abstract void processParameter(IVariableBinding variable, boolean isDeclaration);

	protected abstract void processLocal(IVariableBinding variable, boolean isDeclaration);

	protected abstract void processMethodBinding(IMethodBinding method);

	@Override
	public boolean visit(NullLiteral node) {
		addLiteralParameter(LITERAL_NULL);
		return false;
	}

	@Override
	public boolean visit(StringLiteral node) {
		literals.add(node.getLiteralValue());
		addLiteralParameter(LITERAL_STRING);
		return false;
	}

	@Override
	public boolean visit(NumberLiteral node) {
		literals.add(node.getToken());
		addLiteralParameter(LITERAL_NUMBER);
		return false;
	}

	@Override
	public boolean visit(BooleanLiteral node) {
		literals.add(node.booleanValue());
		addLiteralParameter(LITERAL_BOOLEAN);
		return false;
	}

	@Override
	public boolean visit(CharacterLiteral node) {
		literals.add(node.charValue());
		addLiteralParameter(LITERAL_CHAR);
		return false;
	}

	@Override
	public boolean visit(TypeLiteral node) {
		literals.add(node.getType());
		addLiteralParameter(LITERAL_TYPE);
		return false;
	}

	/*---------------------------------IGNOROVANE EXPRESSIONS---------------------------------------------*/

	@Override
	public boolean visit(ThisExpression node) {
		// ignore
		return false;
	}

}
