/**
 * 
 */
package ajrs.builder;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;

import ajrs.data.AjrsUnit;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsProcessor {

	private final ASTParser astParser = ASTParser.newParser(AST.JLS4);
	private final IFile file;
	private final ICompilationUnit compilationUnit;
	private AjrsUnit ajrsUnit = null;

	private ASTNode root;

	/**
	 * @param file
	 *            file to be processed
	 * @throws JavaModelException
	 *             exception
	 */
	public AjrsProcessor(IFile file) throws JavaModelException {
		this.file = file;
		this.compilationUnit = JavaCore.createCompilationUnitFrom(file);
	}

	public AjrsProcessor(ICompilationUnit compilationUnit) {
		this.compilationUnit = compilationUnit;
		this.file = (IFile) compilationUnit.getResource();
	}

	public void parse(IProgressMonitor monitor) {
		System.err.println("Parsing: " + compilationUnit.getResource().getFullPath());
		astParser.setKind(ASTParser.K_COMPILATION_UNIT);
		astParser.setResolveBindings(true);

		astParser.setSource(compilationUnit);
		this.root = astParser.createAST(monitor);

		ASTVisitor visitor = new ASTVisitor() {
			@Override
			public boolean visit(CompilationUnit node) {
				assert ajrsUnit == null : "Only one Compilation unit should be visited";
				AjrsUnit ajrsUnit = new AjrsUnit(node, file);
				ajrsUnit.visitPeer();
				return false;
			}
		};
		this.root.accept(visitor);
	}

}
