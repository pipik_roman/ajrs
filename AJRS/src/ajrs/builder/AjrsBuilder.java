package ajrs.builder;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.JavaModelException;

import ajrs.data.smells.AjrsSmellCandidate;

public class AjrsBuilder extends IncrementalProjectBuilder {
	public static Map<IMarker, AjrsSmellCandidate> markerToSmell = new HashMap<IMarker, AjrsSmellCandidate>();

	/**
	 * Sluzi na spracovanie pri zmene
	 * 
	 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
	 */
	class DeltaVisitor implements IResourceDeltaVisitor {
		IProgressMonitor monitor;

		public DeltaVisitor(IProgressMonitor monitor) {
			this.monitor = monitor;
		}

		@Override
		public boolean visit(IResourceDelta delta) throws CoreException {
			IResource resource = delta.getResource();
			switch (delta.getKind()) {
			case IResourceDelta.ADDED:
			case IResourceDelta.CHANGED:
				processFile(resource, monitor);
				break;
			}

			// return true to continue visiting children.
			return true;
		}
	}

	/**
	 * Sluzi na spracovanie pri builder
	 * 
	 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
	 */
	class ResourceVisitor implements IResourceVisitor {
		IProgressMonitor monitor;

		public ResourceVisitor(IProgressMonitor monitor) {
			this.monitor = monitor;
		}

		@Override
		public boolean visit(IResource resource) {
			try {
				processFile(resource, monitor);
			} catch (JavaModelException e) {
				e.printStackTrace();
			}
			return true;
		}
	}

	public static final String BUILDER_ID = "AJRS.ajrs.AjrsBuilder";

	/**
	 * Process file
	 * 
	 * @param resource
	 *            resource to process
	 * @param monitor
	 *            monitor of processing
	 * @throws JavaModelException
	 *             on error
	 */
	protected void processFile(IResource resource, IProgressMonitor monitor) throws JavaModelException {
		if ((!(resource instanceof IFile)) || !resource.getName().endsWith(".java")) {
			return;
		}
		IFile file = (IFile) resource;
		AjrsProcessor processor = new AjrsProcessor(file);
		processor.parse(monitor);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.internal.events.InternalBuilder#build(int, java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	protected IProject[] build(int kind, @SuppressWarnings("rawtypes") Map args, IProgressMonitor monitor) throws CoreException {
		if (kind == FULL_BUILD) {
			fullBuild(monitor);
		} else {
			IResourceDelta delta = getDelta(getProject());
			if (delta == null) {
				fullBuild(monitor);
			} else {
				incrementalBuild(delta, monitor);
			}
		}
		return null;
	}

	protected void fullBuild(IProgressMonitor monitor) throws CoreException {
		try {
			getProject().accept(new ResourceVisitor(monitor));
		} catch (CoreException e) {
			// ignore errors
		}
	}

	protected void incrementalBuild(IResourceDelta delta, IProgressMonitor monitor) throws CoreException {
		delta.accept(new DeltaVisitor(monitor));
	}
}
