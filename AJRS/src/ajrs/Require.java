package ajrs;

import java.util.Arrays;

import org.eclipse.jdt.core.dom.ASTNode;

import ajrs.data.AjrsObject;

public class Require {

	public static void requireOneOf(Object any, String name, Object... oneOf) {
		if (any == null) {
			for (Object oneOfObject : oneOf) {
				if (oneOfObject == null) {
					return;
				}
			}
		}
		for (Object oneOfObject : oneOf) {
			if (any.equals(oneOfObject)) {
				return;
			}
		}
		throw new IllegalArgumentException("Illegal value of " + name + "! Actual value: '" + any + "', expected one of: " + Arrays.toString(oneOf));
	}

	public static void notNull(AjrsObject<? extends ASTNode> node, String name) {
		if (node != null) {
			return;
		}
		throw new NullPointerException(name + " cannot be null!");
	}

	public static void requireState(boolean require, String string) {
		if (require) {
			return;
		}
		throw new IllegalStateException(string);
	}
}
