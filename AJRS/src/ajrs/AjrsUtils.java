/**
 * 
 */
package ajrs;

import java.util.Collection;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class AjrsUtils {

	/**
	 * Zmení Collection<Integer> na int[]
	 * 
	 * @param collection
	 *            collection of integers
	 * @return array
	 */
	public static int[] asIntArray(Collection<Integer> collection) {
		int[] array = new int[collection.size()];
		int index = 0;
		for (Integer i : collection) {
			array[index++] = i;
		}
		return array;
	}

	/**
	 * Zmení Collection<Long> na long[]
	 * 
	 * @param collection
	 *            collection of integers
	 * @return array
	 */
	public static long[] asLongArray(Collection<Long> collection) {
		long[] array = new long[collection.size()];
		int index = 0;
		for (Long i : collection) {
			array[index++] = i;
		}
		return array;
	}

}
